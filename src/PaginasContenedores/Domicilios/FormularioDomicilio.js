export default function FormularioDomicilio(props) {
    return (
        <div>
            <h33>Domicilio</h33>
            <div className="row secondRow">
                <div className="col-xs-6 col-sm-4">
                    <h55>
                        <font color="#d5007f">*</font>Calle
          </h55>
                    <InputINE
                        className="ant-input-sinIconoIzquierdo"
                        onSelectLanguage={props.handleInput}
                        label=""
                        name="calle"
                        placeHolder="Ingresa calle"
                        value={props.calle != null ? props.calle : ""}
                        type="text"
                        regex={/^([\w. áéíóúÁÉÍÓÚñ.,-;#’/])+$/}
                        warningMessage="Calle con caracteres inválidos"
                        tooltipMessage="Calle"
                        paddingLeft="3px"
                        disabled={props.disableInputs}
                    />
                </div>
                <div className="col-xs-6 col-sm-2">
                    <div>
                        <h55>
                            <font color="#d5007f">*</font>No. exterior
            </h55>
                        {props.checkBoxNum | props.disableInputs ?
                            <div className="disabled">
                                <InputINE
                                    disabled={true}
                                    onSelectLanguage={props.handleInput}
                                    label=""
                                    name="numeroExt"
                                    placeHolder="Número ext."
                                    value={'S/N'}
                                    type="text"
                                    regex={/^([\w. áéíóúÁÉÍÓÚñ.,-;#’/])+$/}
                                    warningMessage="Verifica el No. exterior"
                                    tooltipMessage="Número exterior"
                                    paddingLeft="3px"
                                /> </div>
                            : (
                                <InputINE
                                    disabled={props.checkBoxNum}
                                    onSelectLanguage={props.handleInput}
                                    label=""
                                    name="numeroExt"
                                    placeHolder="Número ext."
                                    value={convierteNo(props.numeroExterior)}
                                    type="text"
                                    regex={/^([\w. áéíóúÁÉÍÓÚñ.,-;#’/])+$/}
                                    warningMessage="Verifica el No. exterior"
                                    tooltipMessage="Número exterior"
                                    paddingLeft="3px"
                                    disabled={props.disableInputs}
                                />)}
                    </div>
                </div>
                <div className="col-xs-6 col-sm-2">
                    <div>
                        <h55>No. interior</h55>
                        {props.checkBoxNum | props.disableInputs ? <div className="disabled">
                            <InputINE
                                disabled={true}
                                onSelectLanguage={props.handleInput}
                                name="numeroInt"
                                placeHolder="Número int."
                                value={'S/N'}
                                type="text"
                                regex={/^([\w. áéíóúÁÉÍÓÚñ.,-;#’/])+$/}
                                warningMessage="Verifica el No. interior"
                                tooltipMessage="Número interior"
                                paddingLeft="3px"
                            />
                        </div> : (
                                <InputINE
                                    disabled={props.checkBoxNum}
                                    onSelectLanguage={props.handleInput}
                                    label=""
                                    name="numeroInt"
                                    placeHolder="Número int."
                                    value={convierteNo(props.numeroInterior)}
                                    type="text"
                                    regex={/^([\w. áéíóúÁÉÍÓÚñ.,-;#’/])+$/}
                                    warningMessage="Verifica el No. interior"
                                    tooltipMessage="Número interior"
                                    paddingLeft="3px"
                                    disabled={props.disableInputs}
                                />
                            )}
                    </div>
                </div>
                {props.disableInputs ? <div className="col-xs-6 col-sm-2" /> : (
                    <div className="col-xs-6 col-sm-2">
                        <Checkbox
                            style={{ paddingTop: "36px" }}
                            onChange={props.onChangeCheckBoxNum}
                            disabled={props.disableInputs}
                            checked={props.checkBoxNum}
                        >
                            <h45>Sin número</h45>
                        </Checkbox>
                    </div>)
                }
                <div className="col-xs-6 col-sm-2">
                    <h55>
                        <font color="#d5007f">*</font>C.P.
          </h55>
                    <InputINE
                        onSelectLanguage={props.handleInput}
                        label=""
                        name="codigoPostal"
                        placeHolder="12345"
                        value={props.codigoPostal != null ? props.codigoPostal : ""}
                        type="text"
                        regex={/^\d{5}$/}
                        warningMessage="Verifica tu código postal"
                        tooltipMessage="Código Postal"
                        paddingLeft="3px"
                        style={{ disabled: true }}
                        disabled={props.disableInputs}
                    />
                </div>
            </div>
            <div className="form-row mt-4">
                <div className="col-sm-4 pb-3">
                    <div>
                        <h55>
                            <font color="#d5007f">*</font>Entidad Federativa
            </h55>
                        {props.disableCPInputs ? (
                            <div className="disabled">
                                <InputINE
                                    value={props.entidad}
                                    tooltipMessage="Entidad"
                                    paddingLeft="3px"
                                    className=""
                                    placeHolder="Ingresa un código postal válido"
                                    validationIcon=""
                                    regex={/^([\w. áéíóúÁÉÍÓÚñ.,-;#’/])+$/}
                                    onSelectLanguage={props.handleInput}
                                    disabled={true}
                                />
                            </div>
                        ) : (
                                <InputINE
                                    onSelectLanguage={props.handleInput}
                                    label=""
                                    name="entidad"
                                    placeHolder="Ingresa Entidad"
                                    type="text"
                                    regex={/^([\w. áéíóúÁÉÍÓÚñ.,-;#’/])+$/}
                                    value={props.entidad != null ? props.entidad : ""}
                                    warningMessage="Entidad tiene caracteres inválidos"
                                    tooltipMessage="Entidad "
                                    paddingLeft="3px"
                                    disabled={props.disableCPInputs}
                                />
                            )}

                    </div>
                </div>

                <div className="col-sm-4 pb-3">
                    <div>
                        <h55>
                            <font color="#d5007f">*</font>Municipio
            </h55>
                        {props.disableCPInputs ? (
                            <div className="disabled">
                                <InputINE
                                    onSelectLanguage={props.handleInput}
                                    value={props.municipio}
                                    regex={/^([\w. áéíóúÁÉÍÓÚñ.,-;#’/])+$/}
                                    tooltipMessage="Municipio"
                                    placeHolder="Ingresa un código postal válido"
                                    paddingLeft="3px"
                                    className=""
                                    validationIcon=""
                                    disabled={true}
                                />
                            </div>
                        ) : (
                                <InputINE
                                    onSelectLanguage={props.handleInput}
                                    label=""
                                    name="municipio"
                                    placeHolder="Ingresa Municipio"
                                    type="text"
                                    regex={/^([\w. áéíóúÁÉÍÓÚñ.,-;#’/])+$/}
                                    value={props.municipio != null ? props.municipio : ""}
                                    warningMessage="Municipio tiene caracteres inválidos"
                                    tooltipMessage="Municipio"
                                    paddingLeft="3px"
                                    disabled={props.disableCPInputs}
                                ></InputINE>
                            )}
                    </div>
                </div>
                <div className="col-sm-4 pb-3">
                    <h55>
                        <font color="#d5007f">*</font>Colonia o localidad
          </h55>
                    {props.apiCodigo && props.colonias != null ? (
                        <Select style={{ width: "100%" }} onChange={props.selectColonia}>
                            {props.colonias.map((tem) => (
                                <Select.Option key={tem} value={tem}>
                                    <h45>{tem}</h45>
                                </Select.Option>
                            ))}
                        </Select>
                    ) : (
                            <InputINE
                                onSelectLanguage={props.handleInput}
                                label=""
                                name="colonia"
                                placeHolder={props.disableCPInputs ? "Ingresa un código postal válido" : "Ingresa Colonia"}
                                type="text"
                                value={props.colonia != null ? props.colonia : ""}
                                regex={/^([\w. áéíóúÁÉÍÓÚñ.,-;#’/])+$/}
                                warningMessage="Colonia tiene caracteres inválidos"
                                tooltipMessage="Colonia"
                                paddingLeft="3px"
                                disabled={props.disableCPInputs}
                            />
                        )}
                </div>
            </div>
        </div>
    )
}