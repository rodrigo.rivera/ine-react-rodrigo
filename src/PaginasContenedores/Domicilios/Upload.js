/**
 * @author Miguel Ángel García Calderón <mgcalderon@outlook.es>
 */
import React from "react";
import { Upload, Icon, Modal } from "antd";
import Compressor from "compressorjs";

function getBase64(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = (error) => reject(error);
  });
}
class PicturesWall extends React.Component {

  state = {
    previewVisible: false,
    previewImage: "",
    fileList: this.props.editar ? this.props.images : [],
    files: [],
    getFileList: false,
    compressedImages: [],
    names: [],
    promises: [],
    base64: []
  };

  handleCompress = async (file) => {
    if (!file.url && !file.preview) {
      file.preview = await getBase64(file.originFileObj);
    }

  }

  componentWillUpdate() {

  }
  componentDidMount() {
    console.log("DID MOUNT")
    console.log(this.props.images)

  }

  componentDidUpdate() {
    if (this.state.getFileList !== this.props.getFileList) {
      let availableImages = this.getAvailableImages(this.state.names, this.state.compressedImages)
      this.props.callBackRecibeArchivos(this.state.base64, this.state.names, availableImages)
    }
  }

  getAvailableImages(validNames, fileList) {
    let result = []
    fileList.forEach(element => validNames.includes(element.name) ? result.push(element) : null);
    return result
  }
  handleCancel = () => this.setState({ previewVisible: false });

  handlePreview = async (file) => {
    if (!file.url && !file.preview) {
      file.preview = await getBase64(file.originFileObj);
    }

    this.setState({
      previewImage: file.url || file.preview,
      previewVisible: true,
      previewTitle:
        file.name || file.url.substring(file.url.lastIndexOf("/") + 1),
    });
  };
  // removeName = (name, fileList) => {
  //   return fileList.forEach((file) => {
  //     if (file.name == name) fileList.pop(file);
  //   });
  // };
  handleChange = ({ fileList }) => {
    this.setState({ fileList });
    let namesChange = []

    fileList.forEach(file => {
      namesChange.push(file.name)
    })
    this.setState({
      names: namesChange
    })
    console.log("NUEVA LISTA ARCHIVOS")
    console.log(fileList)
  };
  handleupload = (file, fileList) => {
    this.state.files.push(file);
    this.callbackFunction(file);
  };
  dummyRequest = ({ file, onSuccess }) => {
    setTimeout(() => {
      onSuccess("ok");
    }, 0);
  };

  //obtiene los datos desde componente PicturesWall
  calulateNewHeight = (originalWidth, originalHeight, targetWidth) => {
    const ratio = originalHeight / originalWidth;
    return ratio * targetWidth;
  };

  compress(file) {
    let result = new Promise((resolve, reject) => {
      let img = new Image();
      img.src = window.URL.createObjectURL(file);
      //img.onload = () => {
      const newHeight = this.calulateNewHeight(img.width, img.height, 780);
      new Compressor(file, {
        width: 780,
        height: newHeight,
        quality: 0.8,
        success(result) {
          this.setState({ promises: this.state.promises.concat(result) })
          resolve(result)
        },
        error(e) {
          console.log("ERROR AL COMPRIMIR")
          reject(e)
        },
        pending() {
          console.log("pending")
        },
      });
      //}
    })

    return result
  }
  callbackFunction = (fileObj) => {
    let result = new Promise((resolve, reject) => {
      let img = new Image();
      img.src = window.URL.createObjectURL(fileObj);
      img.onload = () => {
        const newHeight = this.calulateNewHeight(img.width, img.height, 780);
        new Compressor(fileObj, {
          width: 780,
          height: newHeight,
          quality: 0.8,
          success(result) {
            resolve(result)
          },
          error(err) {
            console.log(err.message);
            console.log("ERROR AL COMPRIMIR")
            reject(err)
          },
        });
      }
    })
    result.then((value) => {
      this.setState({ compressedImages: this.state.compressedImages.concat(value) })
      this.blobToBase64(value, imageUrl => {
        //setImageUrl(imageUrl)
        console.log(imageUrl)
        this.setState({ base64: this.state.base64.concat(imageUrl) })
        //setLoading(false)
      })
      //this.setState({ compressedImages: this.state.compressedImages.concat(value) })
      //})
      // this.setState({ promises: this.state.promises.concat(result) })
    })
  }

  blobToBase64 = function (blob, callback) {
    var reader = new FileReader();
    reader.onload = function () {
      var dataUrl = reader.result;
      var base64 = dataUrl.split(',')[1];
      callback(base64);
    };
    reader.readAsDataURL(blob);
  };
  getBase64(img, callback) {
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result));
    reader.readAsDataURL(img);
  }

  render() {
    const { previewVisible, previewImage, fileList } = this.state;
    const uploadButton = (
      <div>
        <Icon type="plus" />
        <div className="ant-upload-text">Fotografía</div>
      </div>
    );
    return (
      <div className="clearfix" >
        <Upload
          // action={this.handleUpload} //"//jsonplaceholder.typicode.com/posts/"
          listType="picture-card"
          fileList={fileList}
          onPreview={this.handlePreview}
          onChange={this.handleChange}
          customRequest={this.dummyRequest}
          beforeUpload={this.handleupload}
          accept=".jpg,.jpeg,.bmp,.png,.tiff"
        >
          {fileList.length >= 3 ? null : uploadButton}
        </Upload>
        <Modal
          visible={previewVisible}
          footer={null}
          onCancel={this.handleCancel}
        >
          <img alt="example" style={{ width: "100%" }} src={previewImage} />
        </Modal>
      </div>
    );
  }
}

// ReactDOM.render(<PicturesWall />, mountNode);
export default PicturesWall;
