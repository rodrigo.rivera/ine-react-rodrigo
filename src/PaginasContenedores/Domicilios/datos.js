export const sedes = [
    { id: 1, name: 'Bodega' },
    { id: 2, name: 'Módulo de Atención Ciudadana' },
    { id: 3, name: 'Junta Distrital' },
    { id: 4, name: 'V. de capacitación electoral y educación cívica' },
    { id: 5, name: 'V. de organización electoral' },
    { id: 6, name: 'V. del registro federal de electores' },
    { id: 7, name: 'V. del secretariado' },
    { id: 8, name: 'V. Ejecutiva' },
];