import React, { useState, useEffect } from "react";
import { Checkbox, Select } from "antd";
import PicturesWall from "./Upload.js";
import { Button, Radio } from "antd";
import InputINE from "../../Components/Input/inputINE";
import { getCodigoPolstal } from "../../Components/CodigoPostal/CodigoPostal";
import alertINE from "../../Components/Alert/AlertsINE";
import { useHistory } from "react-router-dom";
import { sedes } from './datos'
import { convierteNo, sedesDisponibles, enviar } from './funciones.js'
import "./style.scss";

export default function AcreditacionCrear(props) {
  const history = useHistory();
  let { menu, editar, match } = props;
  const [listaDomicilios] = useState(JSON.parse(localStorage.getItem("listaDomicilios")))
  const [juntaEjecutiva, setJuntaEjecutiva] = useState(null)
  const [disableInputs, setDisableInputs] = useState(false)
  const [disableCPInputs, setDisableCPInputs] = useState(false)
  const [getFileList, setGetFileList] = useState(false)
  const [clickEnviarForm, setClickEnviarForm] = useState(false)

  const [nombre, setNombre] = useState(domicilioEditar != null ? domicilioEditar.nombre : '')
  const [aPaterno, setAPaterno] = useState(domicilioEditar != null ? domicilioEditar.aPaterno : '')

  const [fileList, setFileList] = useState([]);
  const [fileNames, setFileNames] = useState([])
  console.log("los props Domicilios CREAR ......... ", props);
  const [domicilioEditar] = useState(
    props.editar ? JSON.parse(localStorage.getItem("domicilioEditar"))[0] : editar
  );
  const [fotosDomicilio] = useState(editar ? JSON.parse(localStorage.getItem("domicilioEditar"))[0].images : null)

  const [numeroExterior, setNumExt] = useState(
    domicilioEditar != null ? domicilioEditar.numeroExterior : ''
  );
  const [numeroInterior, setNumInt] = useState(
    domicilioEditar != null ? domicilioEditar.numeroInterior : ''
  );

  const [sede, setSede] = useState(
    editar ? domicilioEditar.sedes.idSede : ''
  );
  const [calle, setCalle] = useState(
    domicilioEditar != null ? domicilioEditar.calle : editar
  );
  const [codigoPostal, setCodigoPostal] = useState(
    domicilioEditar != null ? domicilioEditar.codigoPostal : editar
  );
  const [entidad, setEntidad] = useState(
    domicilioEditar != null ? domicilioEditar.entidad : ''
  );
  const [municipio, setMunicipio] = useState(
    domicilioEditar != null ? domicilioEditar.municipio : editar
  );
  const [colonia, setColonia] = useState(
    domicilioEditar != null ? domicilioEditar.colonia : editar
  );
  const [colonias, setColonias] = useState(
    domicilioEditar != null ? null : null
  );
  const [apiCodigo, setApiCodigo] = useState(
    domicilioEditar != null ? domicilioEditar.apiDireccion : false
  );
  // const [distrito, setDistrito] = useState(null);
  const [checkBoxNum, setCheckBoxNum] = useState(
    (domicilioEditar != null) ? (convierteNo(domicilioEditar.numeroExterior) === 'S/N' ? true : false) : false
  );


  const [formularioSede, setFormularioSede] = useState(editar);
  const [formularioCalle, setFormularioCalle] = useState(editar);
  const [formularioNumeroExterior, setFormularioNoExterior] = useState(editar);
  const [formularioNumeroInterior, setFormularioNoInterior] = useState(editar);
  const [formularioCodigoPostal, setFormularioCodigoPostal] = useState(editar);
  const [formularioEntidad, setFormularioEntidad] = useState(editar);
  const [formularioMunicipio, setFormularioMunicipio] = useState(editar);
  const [formularioColonia, setFormularioColonia] = useState(editar);
  const [formularioFotos] = useState(editar);

  const [domDentroDeJunta, setDomDentroDeJunta] = useState(
    domicilioEditar != null ? domicilioEditar.domDentroDeJunta : 2
  );
  const [tipoModulo, setTipoModulo] = useState(
    domicilioEditar != null ? domicilioEditar.tipoModulo : 1
  );

  /********
   * 
   * 
   * */


  /**
   * 
   */
  const checkIfJuntaEjecutivaRegistrada = () => {
    // console.log("LLAMADA_::::::::::::::*****************************")
    if (listaDomicilios != null) {
      // console.log("YA HAY JUNTA DISTRITAL")
      let res = listaDomicilios.filter(domicilio => domicilio.sedes.idSede === 3)
      setJuntaEjecutiva(res[0])
      // console.log("_:_:::::::::::::::")
      // console.log(res)
      return res.length > 0
    }
    else {
      console.log("NO PASO NO HAY JUNTA DISTRITAL")
    }
    return false
  }
  useEffect(() => {
    // console.log("MENU CAPTURA")
    // console.log(menu)
    // if (menu.infoMenu.distritoFedSelec === null &&
    //   menu.infoMenu.distritoLocSelec === null) {
    //   window.location.href = localStorage.getItem("junta");
    // }
    // console.log("LOCAL STORGAE")
    // console.log(JSON.parse(localStorage.getItem("domicilioEditar")))
    // console.log("FOTOS:::::::")
    // console.log(fotosDomicilio)
    // console.log()
    // if (clickEnviarForm && !getFileList) {
    //   // console.log("ENVIAR FOMRULARIO")
    //   //downloadFile()
    //   enviar(props, history, domicilioEditar, menu, formularioSede,
    //     formularioCalle, formularioNumeroExterior, formularioNumeroInterior, formularioFotos,
    //     formularioCodigoPostal, formularioEntidad, formularioColonia, formularioMunicipio,
    //     calle, entidad, colonia, checkBoxNum, numeroExterior, numeroInterior, codigoPostal,
    //     municipio, domDentroDeJunta, apiCodigo, sede, sedes, tipoModulo, editar, match, fileList, fileNames)
    // }
    // else {
    //   // console.log("No pasó enviar formulario")
    // }
  }, [getFileList]); // Solo se vuelve a ejecutar si count cambia

  useEffect(() => {
    checkIfJuntaEjecutivaRegistrada()
    // console.log("YA HAY JUNTA DISTRITAL")
  });

  const selectSede = (e) => {
    setSede(e);
    setFormularioSede(true);
  };
  const selectColonia = (e) => {
    setColonia(e);
    setFormularioColonia(true);
  };

  const onChangeCheckBoxNum = (e) => {
    setCheckBoxNum(e.target.checked);
    if (checkBoxNum) {
      setNumExt('');
      setNumInt('');

    } else {
      setNumExt('S/N');
      setNumInt('S/N');
    }
  };



  /**
   * redirecciona a
   */
  const cancelar = () => {
    history.push("/DomicilioJunta/Home")
  };

  /**
   * inserta el un dato dependiendo  si edita o es nuevo
   */
  //
  const handleInput = (content, id, name, regex, warningMessage, disabled) => {
    //console.log("disabled pppp " + disabled)
    //setCheckBoxNum(disabled)
    //console.log(regex)
    var regexResult = regex.test(content);

    if (!regexResult) {
      //console.log("warning" + warningMessage)
      alertINE("warning", warningMessage);
      switch (name) {
        case "calle":
          setFormularioCalle(false);
          break;
        case "numeroExt":
          setFormularioNoExterior(false);
          break;
        case "numeroInt":
          setFormularioNoInterior(false);
          break;
        case "codigoPostal":
          setFormularioCodigoPostal(false);
          setFormularioColonia(false);
          setFormularioEntidad(false);
          setFormularioMunicipio(false);
          setColonia('');
          setMunicipio('');
          setEntidad('');
          setApiCodigo(null);
          setDisableCPInputs(true);
          break;
        case "entidad":
          setFormularioEntidad(false);
          break;
        case "municipio":
          setFormularioMunicipio(false);
          break;
        case "colonia":
          setFormularioColonia(false);
          break;
        default:
          break;

      }
    } else {
      switch (name) {
        case "calle":
          setCalle(content);
          setFormularioCalle(true);
          break;
        case "numeroExt":
          setNumExt(content);
          setFormularioNoExterior(true);
          break;
        case "numeroInt":
          setNumInt(content);
          setFormularioNoInterior(true);
          break;
        case "codigoPostal":
          setCodigoPostal(content);
          setApiCodigo(null);
          setEntidad('');
          setMunicipio('');
          setColonia('');

          //console.log('**********************' + content + ' exito'
          setFormularioCodigoPostal(true);

          getCodigoPolstal(content).then((data) => {
            if (!data.error) {
              console.log("::::::::::::se encontro ene el api");
              setEntidad(data.entidad);
              setMunicipio(data.municipio);
              setColonias(data.colonias);
              setApiCodigo(true);
              setDisableCPInputs(true)
              setFormularioMunicipio(true);
              setFormularioColonia(true);
              setFormularioEntidad(true);
            } else {
              console.log(":::::::::::::no se encontro en el api ");
              setEntidad("");
              setMunicipio("");
              setColonias("");
              setApiCodigo(false);
              setDisableCPInputs(false)
              alertINE(
                "info",
                "Código postal no encontrado. Ingresa entidad, municipio y colonia"
              );
            }
          });

          break;
        case "entidad":
          setEntidad(content);
          setFormularioEntidad(true);
          break;
        case "municipio":
          setMunicipio(content);
          setFormularioMunicipio(true);
          break;
        case "colonia":
          setColonia(content);
          setFormularioColonia(true);
          break;
        default:
          break;
      }
    }
  };
  const loadInfoJuntaEjecutiva = () => {
    setCalle(juntaEjecutiva.calle)
    if (juntaEjecutiva.numeroExterior === null | juntaEjecutiva.numeroExterior === '') {
      setNumExt('S/N')
    } else {
      setNumExt(juntaEjecutiva.numeroExterior)
    }
    if (juntaEjecutiva.numeroInterior === null | juntaEjecutiva.numeroInterior === '') {
      setNumInt('S/N')
    }
    else {
      setNumInt(juntaEjecutiva.numeroInterior)
    }
    setEntidad(juntaEjecutiva.entidad)
    setMunicipio(juntaEjecutiva.municipio)
    setColonia(juntaEjecutiva.colonia)
    setCodigoPostal(juntaEjecutiva.codigoPostal)
    setDisableInputs(true)
    setDisableCPInputs(true)
  }
  const cleanForm = () => {
    setCalle('')
    setNumExt('')
    setNumInt('')
    setMunicipio('')
    setEntidad('')
    // setDistrito('')
    setColonia('')
    setCodigoPostal('')
    setDisableInputs(false)
    setDisableCPInputs(false)
  }
  const setFormularioAVerdarero = (value) => {
    setFormularioCalle(value)
    setFormularioNoExterior(value)
    setFormularioNoInterior(value)
    setFormularioCodigoPostal(value)
    setFormularioEntidad(value)
    setFormularioMunicipio(value)
    setFormularioColonia(value)
  }
  const onChangeRadio = (e) => {
    // console.log("radio checked", e.target.value);
    switch (e.target.value) {
      case 1:
        setDomDentroDeJunta(true);
        loadInfoJuntaEjecutiva()
        setFormularioAVerdarero(true)

        break;
      case 2:
        setDomDentroDeJunta(false);
        cleanForm()
        break;
      case 3:
        setTipoModulo(0);
        break;
      case 4:
        setTipoModulo(1);
        break;
      default:
        break;
    }
  };

  const callBackRecibeArchivos = (base64, names, compressedImages) => {
    // console.log("RECIBI :::::::::::::::::::::")
    //console.log(compressedImages)
    // console.log(names)

    // console.log(compressedImages)
    if (editar) {
      console.log("RECIBI :::::::::::::::::::::")
      console.log(compressedImages)
      console.log(names)
    }
    setFileList(compressedImages)
    setFileNames(names)
    setGetFileList(false)
  }
  const enviarFormulario = () => {
    if (!formularioNumeroInterior) {
      setNumInt('S/N')
    }
    setGetFileList(true)
    setClickEnviarForm(true)
  }

  const componenteDomicilio = () => {
    return (
      <div>
        <h33>Domicilio</h33>
        <div className="row secondRow">
          <div className="col-xs-6 col-sm-4">
            <h55>
              <font color="#d5007f">*</font>Calle
          </h55>
            <InputINE
              className="ant-input-sinIconoIzquierdo"
              onSelectLanguage={handleInput}
              label=""
              name="calle"
              placeHolder="Ingresa calle"
              value={calle != null ? calle : ""}
              type="text"
              regex={/^([\w. áéíóúÁÉÍÓÚñ.,-;#’/])+$/}
              warningMessage="Calle con caracteres inválidos"
              tooltipMessage="Calle"
              paddingLeft="3px"
              disabled={disableInputs}
            />
          </div>
          <div className="col-xs-6 col-sm-2">
            <div>
              <h55>
                <font color="#d5007f">*</font>No. exterior
            </h55>
              {checkBoxNum | disableInputs ?
                <div className="disabled">
                  <InputINE
                    disabled={true}
                    onSelectLanguage={handleInput}
                    label=""
                    name="numeroExt"
                    placeHolder="Número ext."
                    value={'S/N'}
                    type="text"
                    regex={/^([\w. áéíóúÁÉÍÓÚñ.,-;#’/])+$/}
                    warningMessage="Verifica el No. exterior"
                    tooltipMessage="Número exterior"
                    paddingLeft="3px"
                  /> </div>
                : (
                  <InputINE
                    onSelectLanguage={handleInput}
                    label=""
                    name="numeroExt"
                    placeHolder="Número ext."
                    value={convierteNo(numeroExterior)}
                    type="text"
                    regex={/^([\w. áéíóúÁÉÍÓÚñ.,-;#’/])+$/}
                    warningMessage="Verifica el No. exterior"
                    tooltipMessage="Número exterior"
                    paddingLeft="3px"
                    disabled={disableInputs}
                  />)}
            </div>
          </div>
          <div className="col-xs-6 col-sm-2">
            <div>
              <h55>No. interior</h55>
              {checkBoxNum | disableInputs ? <div className="disabled">
                <InputINE
                  disabled={true}
                  onSelectLanguage={handleInput}
                  name="numeroInt"
                  placeHolder="Número int."
                  value={'S/N'}
                  type="text"
                  regex={/^([\w. áéíóúÁÉÍÓÚñ.,-;#’/])+$/}
                  warningMessage="Verifica el No. interior"
                  tooltipMessage="Número interior"
                  paddingLeft="3px"
                />
              </div> : (
                  <InputINE
                    onSelectLanguage={handleInput}
                    label=""
                    name="numeroInt"
                    placeHolder="Número int."
                    value={convierteNo(numeroInterior)}
                    type="text"
                    regex={/^([\w. áéíóúÁÉÍÓÚñ.,-;#’/])+$/}
                    warningMessage="Verifica el No. interior"
                    tooltipMessage="Número interior"
                    paddingLeft="3px"
                    disabled={disableInputs}
                  />
                )}
            </div>
          </div>
          {disableInputs ? <div className="col-xs-6 col-sm-2" /> : (
            <div className="col-xs-6 col-sm-2">
              <Checkbox
                style={{ paddingTop: "36px" }}
                onChange={onChangeCheckBoxNum}
                disabled={disableInputs}
                checked={checkBoxNum}
              >
                <h45>Sin número</h45>
              </Checkbox>
            </div>)
          }
          <div className="col-xs-6 col-sm-2">
            <h55>
              <font color="#d5007f">*</font>C.P.
          </h55>
            <InputINE
              onSelectLanguage={handleInput}
              label=""
              name="codigoPostal"
              placeHolder="12345"
              value={codigoPostal != null ? codigoPostal : ""}
              type="text"
              regex={/^\d{5}$/}
              warningMessage="Verifica tu código postal"
              tooltipMessage="Código Postal"
              paddingLeft="3px"
              style={{ disabled: true }}
              disabled={disableInputs}
              maxLength={5}
              number='number'
            />
          </div>
        </div>
        <div className="form-row mt-4">
          <div className="col-sm-4 pb-3">
            <div>
              <h55>
                <font color="#d5007f">*</font>Entidad Federativa
            </h55>
              {disableCPInputs ? (
                <div className="disabled">
                  <InputINE
                    value={entidad}
                    tooltipMessage="Entidad"
                    paddingLeft="3px"
                    className=""
                    placeHolder="Ingresa un código postal válido"
                    validationIcon=""
                    regex={/^([\w. áéíóúÁÉÍÓÚñ.,-;#’/])+$/}
                    onSelectLanguage={handleInput}
                    disabled={true}
                  />
                </div>
              ) : (
                  <InputINE
                    onSelectLanguage={handleInput}
                    label=""
                    name="entidad"
                    placeHolder="Ingresa Entidad"
                    type="text"
                    regex={/^([\w. áéíóúÁÉÍÓÚñ.,-;#’/])+$/}
                    value={entidad != null ? entidad : ""}
                    warningMessage="Entidad tiene caracteres inválidos"
                    tooltipMessage="Entidad "
                    paddingLeft="3px"
                    disabled={disableCPInputs}
                  />
                )}

            </div>
          </div>

          <div className="col-sm-4 pb-3">
            <div>
              <h55>
                <font color="#d5007f">*</font>Municipio
            </h55>
              {disableCPInputs ? (
                <div className="disabled">
                  <InputINE
                    onSelectLanguage={handleInput}
                    value={municipio}
                    regex={/^([\w. áéíóúÁÉÍÓÚñ.,-;#’/])+$/}
                    tooltipMessage="Municipio"
                    placeHolder="Ingresa un código postal válido"
                    paddingLeft="3px"
                    className=""
                    validationIcon=""
                    disabled={true}
                  />
                </div>
              ) : (
                  <InputINE
                    onSelectLanguage={handleInput}
                    label=""
                    name="municipio"
                    placeHolder="Ingresa Municipio"
                    type="text"
                    regex={/^([\w. áéíóúÁÉÍÓÚñ.,-;#’/])+$/}
                    value={municipio != null ? municipio : ""}
                    warningMessage="Municipio tiene caracteres inválidos"
                    tooltipMessage="Municipio"
                    paddingLeft="3px"
                    disabled={disableCPInputs}
                  ></InputINE>
                )}
            </div>
          </div>
          <div className="col-sm-4 pb-3">
            <h55>
              <font color="#d5007f">*</font>Colonia o localidad
          </h55>
            {apiCodigo && colonias != null ? (
              <Select style={{ width: "100%" }} onChange={selectColonia}>
                {colonias.map((tem) => (
                  <Select.Option key={tem} value={tem}>
                    <h45>{tem}</h45>
                  </Select.Option>
                ))}
              </Select>
            ) : (
                <InputINE
                  onSelectLanguage={handleInput}
                  label=""
                  name="colonia"
                  placeHolder={disableCPInputs ? "Ingresa un código postal válido" : "Ingresa Colonia"}
                  type="text"
                  value={colonia != null ? colonia : ""}
                  regex={/^([\w. áéíóúÁÉÍÓÚñ.,-;#’/])+$/}
                  warningMessage="Colonia tiene caracteres inválidos"
                  tooltipMessage="Colonia"
                  paddingLeft="3px"
                  disabled={disableCPInputs}
                />
              )}
          </div>
        </div>
      </div>
    )
  }
  const componenteNombre = () => {
    return (
      <div>
        <h33>Secretario Técnico</h33>
        <div className="row secondRow">
          <div className="col-xs-6 col-sm-4">
            <h55>
              <font color="#d5007f">*</font>Primer apellido
            </h55>
            <InputINE
              className="ant-input-sinIconoIzquierdo"
              onSelectLanguage={handleInput}
              label=""
              name="primerPaterno"
              placeHolder="Apellido paterno"
              value={aPaterno != null ? aPaterno : ""}
              type="text"
              regex={/^([\w. áéíóúÁÉÍÓÚñ.,-;#’/])+$/}
              warningMessage="Apellido paterno con caracteres inválidos"
              tooltipMessage="Apellido paterno"
              paddingLeft="3px"
            />
          </div>
          <div className="col-xs-6 col-sm-2">
            <h55>
              <font color="#d5007f">*</font>Segundo apellido
            </h55>
            <InputINE
              disabled={true}
              onSelectLanguage={handleInput}
              label=""
              name="segundoApellido"
              placeHolder="Número ext."
              value={'S/N'}
              type="text"
              regex={/^([\w. áéíóúÁÉÍÓÚñ.,-;#’/])+$/}
              warningMessage="Verifica el No. exterior"
              tooltipMessage="Número exterior"
              paddingLeft="3px"
            />
          </div>
          <div className="col-xs-6 col-sm-2">
            <div>
              <h55>No. interior</h55>
              {checkBoxNum | disableInputs ? <div className="disabled">
                <InputINE
                  disabled={true}
                  onSelectLanguage={handleInput}
                  name="numeroInt"
                  placeHolder="Número int."
                  value={'S/N'}
                  type="text"
                  regex={/^([\w. áéíóúÁÉÍÓÚñ.,-;#’/])+$/}
                  warningMessage="Verifica el No. interior"
                  tooltipMessage="Número interior"
                  paddingLeft="3px"
                />
              </div> : (
                  <InputINE
                    onSelectLanguage={handleInput}
                    label=""
                    name="numeroInt"
                    placeHolder="Número int."
                    value={convierteNo(numeroInterior)}
                    type="text"
                    regex={/^([\w. áéíóúÁÉÍÓÚñ.,-;#’/])+$/}
                    warningMessage="Verifica el No. interior"
                    tooltipMessage="Número interior"
                    paddingLeft="3px"
                    disabled={disableInputs}
                  />
                )}
            </div>
          </div>
          {disableInputs ? <div className="col-xs-6 col-sm-2" /> : (
            <div className="col-xs-6 col-sm-2">
              <Checkbox
                style={{ paddingTop: "36px" }}
                onChange={onChangeCheckBoxNum}
                disabled={disableInputs}
                checked={checkBoxNum}
              >
                <h45>Sin número</h45>
              </Checkbox>
            </div>)
          }
          <div className="col-xs-6 col-sm-2">
            <h55>
              <font color="#d5007f">*</font>C.P.
          </h55>
            <InputINE
              onSelectLanguage={handleInput}
              label=""
              name="codigoPostal"
              placeHolder="12345"
              value={codigoPostal != null ? codigoPostal : ""}
              type="text"
              regex={/^\d{5}$/}
              warningMessage="Verifica tu código postal"
              tooltipMessage="Código Postal"
              paddingLeft="3px"
              style={{ disabled: true }}
              disabled={disableInputs}
              maxLength={5}
              number='number'
            />
          </div>
        </div>
        <div className="form-row mt-4">
          <div className="col-sm-4 pb-3">
            <div>
              <h55>
                <font color="#d5007f">*</font>Entidad Federativa
            </h55>
              {disableCPInputs ? (
                <div className="disabled">
                  <InputINE
                    value={entidad}
                    tooltipMessage="Entidad"
                    paddingLeft="3px"
                    className=""
                    placeHolder="Ingresa un código postal válido"
                    validationIcon=""
                    regex={/^([\w. áéíóúÁÉÍÓÚñ.,-;#’/])+$/}
                    onSelectLanguage={handleInput}
                    disabled={true}
                  />
                </div>
              ) : (
                  <InputINE
                    onSelectLanguage={handleInput}
                    label=""
                    name="entidad"
                    placeHolder="Ingresa Entidad"
                    type="text"
                    regex={/^([\w. áéíóúÁÉÍÓÚñ.,-;#’/])+$/}
                    value={entidad != null ? entidad : ""}
                    warningMessage="Entidad tiene caracteres inválidos"
                    tooltipMessage="Entidad "
                    paddingLeft="3px"
                    disabled={disableCPInputs}
                  />
                )}

            </div>
          </div>

          <div className="col-sm-4 pb-3">
            <div>
              <h55>
                <font color="#d5007f">*</font>Municipio
            </h55>
              {disableCPInputs ? (
                <div className="disabled">
                  <InputINE
                    onSelectLanguage={handleInput}
                    value={municipio}
                    regex={/^([\w. áéíóúÁÉÍÓÚñ.,-;#’/])+$/}
                    tooltipMessage="Municipio"
                    placeHolder="Ingresa un código postal válido"
                    paddingLeft="3px"
                    className=""
                    validationIcon=""
                    disabled={true}
                  />
                </div>
              ) : (
                  <InputINE
                    onSelectLanguage={handleInput}
                    label=""
                    name="municipio"
                    placeHolder="Ingresa Municipio"
                    type="text"
                    regex={/^([\w. áéíóúÁÉÍÓÚñ.,-;#’/])+$/}
                    value={municipio != null ? municipio : ""}
                    warningMessage="Municipio tiene caracteres inválidos"
                    tooltipMessage="Municipio"
                    paddingLeft="3px"
                    disabled={disableCPInputs}
                  ></InputINE>
                )}
            </div>
          </div>
          <div className="col-sm-4 pb-3">
            <h55>
              <font color="#d5007f">*</font>Colonia o localidad
          </h55>
            {apiCodigo && colonias != null ? (
              <Select style={{ width: "100%" }} onChange={selectColonia}>
                {colonias.map((tem) => (
                  <Select.Option key={tem} value={tem}>
                    <h45>{tem}</h45>
                  </Select.Option>
                ))}
              </Select>
            ) : (
                <InputINE
                  onSelectLanguage={handleInput}
                  label=""
                  name="colonia"
                  placeHolder={disableCPInputs ? "Ingresa un código postal válido" : "Ingresa Colonia"}
                  type="text"
                  value={colonia != null ? colonia : ""}
                  regex={/^([\w. áéíóúÁÉÍÓÚñ.,-;#’/])+$/}
                  warningMessage="Colonia tiene caracteres inválidos"
                  tooltipMessage="Colonia"
                  paddingLeft="3px"
                  disabled={disableCPInputs}
                />
              )}
          </div>
        </div>
      </div >
    )
  }

  return (
    // <div className="container" style={{ padding: "30px" }}>
    <div className="containerLista container">
      {editar ? (
        <h11>Modificar {domicilioEditar.sedes.sede}</h11>
      ) : (
          <h11>Nuevo registro de Comisión</h11>
        )}
      <div className="row">
        <h66>
          Los datos con ( <font color="#d5007f">*</font> ) son requeridos.
        </h66>
      </div>
      <div className="row firstRow">
        <div className="col-sm-4 pb-3">
          <h55>
            <font color="#d5007f">*</font>
              Nombre de la comisión
            </h55>
          <InputINE
            className="ant-input-sinIconoIzquierdo"
            onSelectLanguage={handleInput}
            label=""
            name="nombre"
            placeHolder="Ingresa el nombre"
            value={nombre != null ? nombre : ""}
            type="text"
            regex={/^([\w. áéíóúÁÉÍÓÚñ.,-;#’/])+$/}
            warningMessage="Nombre con caracteres inválidos"
            tooltipMessage="Nombre de la comisión"
            paddingLeft="3px"
          />
        </div>
      </div>

      <div className="row firstRow">
        <div className="col-sm-4 pb-3">
          <h55>
            <font color="#d5007f">*</font>Secretario Técnico
            </h55>
          <InputINE
            className="ant-input-sinIconoIzquierdo"
            onSelectLanguage={handleInput}
            label=""
            name="apellidoPaterno"
            placeHolder="Apellido paterno"
            value={aPaterno != null ? aPaterno : ""}
            type="text"
            regex={/^([\w. áéíóúÁÉÍÓÚñ.,-;#’/])+$/}
            warningMessage="Apellido paterno con caracteres inválidos"
            tooltipMessage="Apellido paterno"
            paddingLeft="3px"
          />
        </div>
        <div className="col-sm-4 pb-3">
          <h55>
            <font color="#d5007f">*</font>Secretario Técnico
            </h55>
          <InputINE
            className="ant-input-sinIconoIzquierdo"
            onSelectLanguage={handleInput}
            label=""
            name="nombre"
            placeHolder="Ingresa el nombre"
            value={nombre != null ? nombre : ""}
            type="text"
            regex={/^([\w. áéíóúÁÉÍÓÚñ.,-;#’/])+$/}
            warningMessage="Nombre con caracteres inválidos"
            tooltipMessage="Nombre de la comisión"
            paddingLeft="3px"
          />
        </div>
      </div>

      {!editar ? (
        <div className="row firstRow">
          <div className="col-sm-4 pb-3">
            <h55>
              <font color="#d5007f">*</font>
              Sede
            </h55>
            <Select
              placeholder="Selecciona"
              value={sede}
              style={{ width: "100%" }}
              dropdownStyle={{}}
              onChange={selectSede}
              filterOption={(inputValue, option) =>
                option.props.children
                  .toString()
                  .toLowerCase()
                  .includes(inputValue.toLowerCase())
              }
            >
              {sedesDisponibles(sedes).map(option => (
                <Select.Option key={option.id} value={option.id}><h45>{option.name}</h45></Select.Option>
              ))}
            </Select>
          </div>
          {juntaEjecutiva != null && formularioSede ? (
            <div className="col-sm-4 pb-3">
              <h55>
                <font color="#d5007f">*</font>Dentro de Junta
            </h55>
              <Radio.Group
                onChange={onChangeRadio}
                value={domDentroDeJunta === true ? 1 : 2}
                style={{ width: "100%" }}
              >
                <Radio value={1}>Si</Radio>
                <Radio value={2}>No</Radio>
              </Radio.Group>
            </div>) : null
          }
          {/* Se activa cuando Sede es "ATENCION CIUDADANA" */}
          {sede === '2' | sede === 2 ? (
            <div className="col-sm-4 pb-3">
              <h55>
                <font color="#d5007f">*</font>Tipo
              </h55>
              <Radio.Group
                onChange={onChangeRadio}
                value={tipoModulo === 0 ? 3 : 4}
                style={{ width: "100%" }}
              >
                <Radio value={3}>Fijo</Radio>
                <Radio value={4}>Móvil</Radio>
              </Radio.Group>
            </div>
          ) : null}
        </div>
      ) : null}
      {/* Sólo activo en creación FIN*/}
      <div style={{ paddingTop: editar ? '15px' : '5px' }}>
        {componenteDomicilio()}
      </div>
      <div className="clearfix">
        <h33>Imágenes del domicilio</h33>
        <p />
        <h55>
          <font color="#d5007f">*</font>Máximo 3 imágenes
        </h55>
        <PicturesWall
          images={fotosDomicilio}
          getFileList={getFileList}
          callBackRecibeArchivos={callBackRecibeArchivos}
          editar={editar}
        />
      </div>

      <div className="row">
        <div className="col-sm-3 pb-3" />
        <div className="col-sm-3 col-xs-6 pb-3">
          <Button
            block
            type="secondary"
            htmlType="reset"
            onClick={cancelar}
            className="login-form-button"
            tabIndex="4"
            style={{ width: "100%", paddingRight: "10px" }}
          >
            Cancelar
          </Button>
        </div>
        <div className="col-sm-3 col-xs-6 pb-3">
          <Button
            block
            type="primary"
            htmlType="submit"
            className="login-form-button"
            onClick={enviarFormulario}
            tabIndex="4"
            style={{ width: "100%" }}
          >
            Guardar
          </Button>
        </div>
        <div className="col-sm-3 pb-3" />
      </div>
    </div>
  );
}
