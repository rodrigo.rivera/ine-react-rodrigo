import React from "react";
import { Button } from "antd";
import { Dialog, DialogContent, DialogActions, DialogTitle } from "@material-ui/core";


export default function DialogoINE(props) {
    return (
        <Dialog open={props.openDialog}>
            <DialogTitle>{props.title}</DialogTitle>
            <DialogContent>{props.content}</DialogContent>
            <DialogActions>
                <Button type="primary" onClick={props.handleAceptar}>Aceptar</Button>
                <Button type="secondary" onClick={props.handleCancelar}>Cancelar</Button>
            </DialogActions>
        </Dialog>
    )
}