import React, { useState, useEffect } from "react"
import { Button, Spin, Icon } from "antd"
import ListaPartidos from "./listaPartidos"
import { getDomicilios } from "../../ActionsService/DomiciliosService"
import { Link } from "react-router-dom"
import { ReactComponent as NewIcon } from '../../Assets/icons/Nuevoreg.svg'

const antIcon = <Icon type="loading" style={{ fontSize: 30 }} spin />;
export default function Acreditacion(props) {
  let { menu, store, storeDomicilios } = props;
  // console.log("PROPS DOMICILIO ")
  // console.log(props)
  console.log("MENU")
  console.log(menu)
  const [loading, setLoading] = useState(true)
  const [loadingAcreditacion, setLoadingAcreditacion] = useState(true);
  const [mensaje, setMensaje] = useState("Cargando Domicilios");
  const [lstDireccionesJuntas, setLstDireccionesJuntas] = useState(null)
  const [fotos, setFotos] = useState(null)

  const [permisos] = useState(
    menu.infoMenu.listMenu.length > 0
      ? menu.infoMenu.listMenu[0].subMenus[0].modulos[1].listAccionesModulo
      : null
  );
  /**
   * obtine los datos de
   * @param {*} temp es el menu
   */
  // useEffect(() => {
  //   console.log("USE EFFECT LISTA DOMICLIOS")
  // })
  useEffect(() => {
    console.log("MENU^^^^^^^^")
    console.log(menu)
    if (menu.infoMenu.estadoSelec === null | !permisos) {
      window.location.href = localStorage.getItem("junta");
    }
    store.dispatch({ type: "LOADED" });
    if (menu.infoMenu.estadoSelec != null) {
      // (async () => {
      localStorage.setItem("waitingResponse", true);
      getDatosAcreditacion(menu);
      // console.log(lstDireccionesJuntas)
      // })();
    }
  }, []);

  store.subscribe(() => {

    let respuestaStore = store.getState();

    if (respuestaStore.pending) {
      setLoading(true)
    }
    else {
      setLoading(false)
    }
  })
  /**
   * obtine los datos de
   * @param {*} temp es el menu
   */
  const getDatosAcreditacion = (temp) => {
    //const temp = JSON.parse(localStorage.getItem("data")).infoMenu;
    //console.log("servicio de listarr ", temp);

    let tempDistrito = null;
    if (temp.infoMenu.distritoFedSelec != null) {
      //console.log(temp.distritoFedSelec.idDistrito)
      tempDistrito = temp.infoMenu.distritoFedSelec.idDistrito;
    }
    if (temp.infoMenu.distritoLocSelec != null) {
      tempDistrito = temp.infoMenu.distritoLocSelec.idDistrito;
    }
    //console.log("---------", temp.detalleSelec != null, "  ", temp.estadoSelec != null, "  ", tempDistrito != null)
    // console.log("CONSULTA CON " + temp.infoMenu.estadoSelec.idEstado + " __ " + tempDistrito)
    if (
      temp.infoMenu.estadoSelec != null &&
      tempDistrito != null
    ) {
      const data = {
        idEstado: temp.infoMenu.estadoSelec.idEstado,
        idDistrito: tempDistrito,
      };
      store.dispatch({ type: "LOADING" });
      setLoadingAcreditacion(true)
      setLoading(true)
      getDomicilios(
        data.idEstado,
        data.idDistrito
      ).then((response) => {
        setMensaje("Cargando Domicilios")
        console.log(response)
        if (!response.error & !response.erorr) {
          setLstDireccionesJuntas(response.data.lstDireccionesJuntas)
          //setFotos(response.data.archivos)
          if (response.data.lstDireccionesJuntas != null) {
            setFotos(base64ToBlobs(response.data.archivos))
            setLoading(true)
          } else {
            setLoading(true)
            console.log("NO VIENEN FOTOS")
          }
          if (JSON.stringify(response.data.lstDireccionesJuntas) !== undefined) {
            localStorage.setItem("listaDomicilios", JSON.stringify(response.data.lstDireccionesJuntas));
          }
          // localStorage.setItem("fotos", JSON.stringify(response.data.archivos));
        } else { setMensaje("Selecciona un distrito") }

        localStorage.setItem("waitingResponse", false);
        store.dispatch({ type: "LOADED" });
        setLoadingAcreditacion(false)
      });
    } else {
      setMensaje("Selecciona un distrito")
      console.log("no paso");
    }
  };

  function b64toBlob(b64Data, contentType) {
    contentType = contentType || '';
    var sliceSize = 512;
    b64Data = b64Data.replace(/^[^,]+,/, '');
    b64Data = b64Data.replace(/\s/g, '');
    var byteCharacters = window.atob(b64Data);
    var byteArrays = [];

    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      var slice = byteCharacters.slice(offset, offset + sliceSize);

      var byteNumbers = new Array(slice.length);
      for (var i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      var byteArray = new Uint8Array(byteNumbers);

      byteArrays.push(byteArray);
    }

    var blob = URL.createObjectURL(new Blob(byteArrays, { type: contentType }));
    return blob;
  }
  const base64ToBlobs = (base64Images) => {
    var images = { blobs: [] }

    for (var i = 0; i < base64Images.length; i++) {
      if (base64Images[i].archivoBase64 !== null) {
        images.blobs.push({
          "source": b64toBlob(base64Images[i].archivoBase64, 'image/jpeg'),
          "name": base64Images[i].nombreArchivo.split('-')[0],
          "order": base64Images[i].nombreArchivo.split('-')[1]
        });
      }

    }
    console.log("IMAGE:::::::::::::::::::::::")
    console.log(images)
    return images
  };

  return (
    <div>
      {loadingAcreditacion | loading ? (
        <div className="center-screen">
          <Spin indicator={antIcon} tip={mensaje}></Spin>
        </div>
      ) : (
          <div className="containerLista container">
            <h11>Domicilio de Junta</h11>
            {
              permisos != null
                ? permisos.map(
                  (temp) =>
                    temp.idAccion === 1 ? (
                      (
                        <div className="row" style={{ paddingTop: "10px" }}>
                          <div className="col-lg-12">
                            <Link to="/DomicilioJunta/Captura">
                              <Button className="btn btn-sq-lg btn-secondaryINE">
                                <NewIcon width="48px" height="48px" style={{ fontSize: "48px", color: "black" }} />
                                <br />
                                <p style={{ fontSize: "16px" }}>Nuevo registro</p>
                              </Button>
                            </Link>
                          </div>
                        </div>
                      )
                    )
                      : null
                ) : null
            }

            <div>
              <div style={{ marginBottom: 16 }}></div>
              {/* {console.log(lstDireccionesJuntas)} */}

              <ListaPartidos
                listaActualPartidos={lstDireccionesJuntas}
                partidos={lstDireccionesJuntas}
                store={storeDomicilios}
                fotos={fotos}
                permisos={permisos}
              //totalPaginas={Math.ceil(lstDireccionesJuntas.length / 5)}
              />
            </div>
          </div>)}
    </div>
  )
}
