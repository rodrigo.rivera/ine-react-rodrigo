import React, { useState } from "react";
import { Layout, Button } from "antd";
import "./Header.scss";
import LogoUser from "../../../Assets/Imagenes/Header/user.svg";
import LogoConsejo from "../../../Assets/Imagenes/Header/Logo.svg";
import { Dialog, DialogContent, DialogActions, DialogTitle } from "@material-ui/core";
import { fetchLogout } from "../../../ActionsService/HomeActionService";
import "antd/dist/antd.css";
import { CloseCircleOutlined } from "@ant-design/icons";

const mapStateToProps = (state) => {
  return {
    inicio: state.inicio,
  };
};

const mapDispatchToProps = (dispatch) => ({
  fetchLogout: (accessToken) => dispatch(fetchLogout(accessToken)),
});

export default function Header(props) {
  //console.log("los props herader ", props)
  const [alertOpen, setAlertOpen] = useState(false);

  const handleClickOpen = () => {
    setAlertOpen(true);
  };

  const handleClose = () => {
    setAlertOpen(false);
  };

  const logout = () => {
    console.log("cierra sesión");
    console.log(props.accessToken);
    setAlertOpen(false);
    fetchLogout(props.accessToken)(null);
  };

  // render () {
  const { Header } = Layout; //Se importa el header del componente del Layout
  //const data = JSON.parse(props);

  const { messageError } = props.inicio;

  if (messageError !== undefined)
    if (messageError != null) {
      alert("ocurrio un error, favor de intentar más tarde.");
    }

  return (
    <Header>
      <div className="menu-top">
        {/*Se crea la estructura del header  */}
        <div className="menu-top__logo">
          <img src={LogoConsejo} className="tam-logo-sis" alt="user" />
        </div>
        <div className="menu-top__ayuda">
          <p>
            {/* {User &&
                                <> */}
            <img src={LogoUser} className="userLogo" alt="user" />
            {props.User}&nbsp;&nbsp;&nbsp;|
            <Button
              className="ant-btn-link hideOnMobile"
              onClick={handleClickOpen}
            >
              <CloseCircleOutlined
                style={{
                  position: "relative",
                  top: "8.5px",
                  transform: "translateY(-69.5%)",
                  fontSize: "15px",
                }}
              />
              Cerrar sesión
            </Button>
            <Dialog open={alertOpen}>
              <DialogTitle id="form-dialog-title">¿Deseas Cerrar Sesión?</DialogTitle>
              <DialogContent>Una vez cerrada la sesión deberás volver a ingresar usuario y contraseña.</DialogContent>
              <DialogActions>
                <Button type="secondary" onClick={handleClose}>Cancelar</Button>
                <Button type="primary" onClick={() => logout()}>Aceptar</Button>
              </DialogActions>
            </Dialog>
            {/* </>
                                } */}
          </p>
        </div>
      </div>
    </Header>
  );
}
