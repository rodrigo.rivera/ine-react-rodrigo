import React from "react";
import { Layout } from "antd";
import "./Footer.scss";
import logoINE from "../../../Assets/Imagenes/Footer/logoINE_bco.svg";

export default function Footer(props) {
  const { Footer } = Layout;

  return (
    <Footer className="footer">
      <div className="footer__instituto">
        <div className="d-md-none">
          <img src={logoINE} alt="Logo" className="center" />
        </div>
        <div className=" d-none d-md-block">
          <img src={logoINE} alt="Logo" />
          <h45>
            <span dangerouslySetInnerHTML={{ __html: "&copy;" }} />
            INE México {props.anio}
            <h45-version>
        		10.0.0 Rev.2 18/05/2020 12:25 
        	</h45-version>
            <a className="CAU_INETEL" href="https://cau.ine.mx/" target="blank">
              CAU
            </a>
          </h45>
        </div>
      </div>
      <div className=" d-none d-md-block footer__area">
        <h45>Unidad Técnica de Servicios Informáticos</h45>
      </div>
      <div className="d-none d-md-block footer__version">
        <h45>
        	Versión del sistema 10.0
        </h45>
      </div>
    </Footer>
  );
}
