import React, { useState, useEffect } from "react";
import { Button, Select, Radio, DatePicker } from 'antd';
import Archivo from "../../Components/Imagenes/cargarArchivos";
import Formulario from "./VocalesFormulario";
import { useParams, Link } from 'react-router-dom';
import axios from "axios";
import alertINE from "../../Components/Alert/AlertsINE";
const { Option, OptGroup } = Select;


export default function VocalesBaja(props) {
    let { id } = useParams();

    const { data, menu } = props;
    const [dataMenu] = useState(props.menu)
    const [asingar, setAsignar] = useState(null)
    const [asignarNuevo, setNuevo] = useState(false);
    const [dataBaja, setDataBaja] = useState(null);
    const [motivos, setMotivos] = useState(null);
    const [sustitutos, setSustitutos] = useState(null);
    const [curp, setCurp] = useState(null);
    const [fechaBaja, setFechaBaja] = useState(null);
    const [vocal, setVocal] = useState(null);
    const [nuevoVocal, setNuevoVocal] = useState(null);
    const [motivo, setMotivo] = useState(null);
    const [Evidencia01, setEvidencia01] = useState(null);
    const [Evidencia02, setEvidencia02] = useState(null);
    const [sustitutoFechaIngreso, setSustitutoFechaIngreso] = useState(null);
    const [sustitutoFechaNombramiento, setSustitutoFechaNombramiento] = useState(null);

    const [tipo, setTipo] = useState(null)
    const [urlImagen, setUrlImagen] = useState(null);


    useEffect(() => {
        if (dataMenu.infoMenu.distritoFedSelec != null && dataMenu.infoMenu.estadoSelec) {


            axios.get(window.location.origin + '/JsonHelpers/jsonUrl.json').then((response) => {
                // console.log("urls::::::::::");
                console.log(response);
                localStorage.setItem('aplicaciones', response.data.urlLogin);
                let token = localStorage.getItem('accessToken');
                console.log('el token para insertar DomiciliosDeJunta: ', token);
                if (response.status === 200) {
                    //setUrls(response.data);

                    let data = {
                        idEstado: dataMenu.infoMenu.estadoSelec.idEstado,
                        idDistrito: dataMenu.infoMenu.distritoFedSelec.idDistrito,
                        modulo: "Vocales",
                        idSubModulo: id,
                        nombreSistema: "junta",
                        modificar: true,
                    }
                    console.log("Mostrar datos ", data)
                    axios.post(`${response.data.centralSesiones}/vocales/descargaArchivo`, data, {
                        headers: {
                            Authorization: token,
                        },
                    })
                        .then((respuesta) => {
                            //localStorage.setItem("accessToken", response.data.token);
                            console.log("la data ", respuesta)
                            let lista = base64ToBlobs(respuesta.data.archivo)
                            console.log(lista.blobs);
                            setUrlImagen(lista.blobs[0].source);
                            // setUrlINombramiento(lista.blobs[1].source);
                            // setUrlIFirma(lista.blobs[2].source);

                        })
                        .catch((erorr) => {
                            console.log('eroror ', erorr);
                        });

                    axios.post(`${response.data.centralSesiones}/vocales/getVocal`, {
                        //"idDetalleProceso": 106,
                        "idDistrito": menu.infoMenu.distritoFedSelec.idDistrito,
                        "idEstado": menu.infoMenu.estadoSelec.idEstado,
                        "idVocal": id
                    }, {
                        headers: {
                            Authorization: token,
                        },
                    })
                        .then((respuesta) => {
                            //localStorage.setItem("accessToken", response.data.token);
                            console.log(respuesta.data)
                            if (respuesta.data.code == 200) {
                                console.log(respuesta.data.vocal)
                                setDataBaja(respuesta.data.vocal);
                                setSustitutos(respuesta.data.sustitulos);
                                setMotivos(JSON.parse(localStorage.getItem("motivos")));
                                //localStorage.setItem("baja", JSON.stringify(respuesta.data.vocal))
                            } else {
                                alertINE("eror", "no se puedo encontrar ese registro ")

                            }
                        })
                        .catch((error) => {
                            alertINE("eror", "Erro en la conexion ")
                            console.log("error ", error)
                        });
                }
            });

        }


    }, []);
    const base64ToBlobs = (base64Images) => {
        console.log("recibe ", base64Images)
        var images = { blobs: [] }

        for (var i = 0; i < base64Images.length; i++) {
            //console.log(base64Images[i].tipoArchivo)
            if (base64Images[i].archivoBase64 !== null) {
                images.blobs.push({
                    "source": b64toBlob(base64Images[i].archivoBase64, base64Images[i].tipoArchivo),
                    "name": base64Images[i].nombreArchivo.split('-')[0],
                    "order": base64Images[i].nombreArchivo.split('-')[1]
                });
            }

        }
        console.log("Archivos:::::::::::::::::::::::")
        console.log(images)
        return images
    };

    function b64toBlob(b64Data, contentType) {
        contentType = contentType || '';
        var sliceSize = 512;
        b64Data = b64Data.replace(/^[^,]+,/, '');
        b64Data = b64Data.replace(/\s/g, '');
        var byteCharacters = window.atob(b64Data);
        var byteArrays = [];

        for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            var slice = byteCharacters.slice(offset, offset + sliceSize);

            var byteNumbers = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }

            var byteArray = new Uint8Array(byteNumbers);

            byteArrays.push(byteArray);
        }

        var blob = URL.createObjectURL(new Blob(byteArrays, { type: contentType }));
        return blob;
    }

    const changeMotivo = e => {
        console.log("Motivo ", e)
        setMotivo(e);
    }
    const changeAsignar = e => {
        //console.log(e)
        //console.log("asignar ", e.target.value)
        setAsignar(e.target.value)
        if (e.target.value) {
            setTipo(1)// Sustituir 
            console.log("Lista ")
        } else {
            setTipo(null)// Sustituir 
            console.log("nuevo  o  pendiente ")
        }
    }

    const changeAsignarNo = e => {
        //console.log(e)
        //console.log("asignar ", e.target.value)
        setNuevo(e.target.value)
        if (e.target.value) {
            //console.log("nuevo");
            setTipo(2)
        } else {
            //console.log("pendiente");
            setTipo(3)
        }
    }

    const changeFechaBaja = (data, dateString) => {
        console.log("fecha Nacimeinto ", dateString)
        setFechaBaja(dateString)
    }

    const changeNombre = e => {
        console.log("Nombre id: ", e)
        //setCurp("null");
        sustitutos.forEach(element => {
            //console.log("Elemnto", element.id_VOCAL, " ==", e)
            if (element.id_VOCAL == e) {
                console.log("exito")
                setCurp(element.curp);
                setVocal(element)
            }
        });
    }

    const changeSustituidoFechaIngreso = (data, dateString) => {
        console.log("fecha Sutituido ", dateString)
        setSustitutoFechaIngreso(dateString)
    }


    const changeSustituidoFechaNombre = (data, dateString) => {
        console.log("fecha Nombramiento ", dateString)
        setSustitutoFechaNombramiento(dateString)
    }



    const evidencia01 = e => {
        console.log("el archivo que se cargo ", e)
        setEvidencia01("binario01")
    }
    const evidencia02 = e => {
        console.log("el archivo que se cargo ", e)
        setEvidencia02("binario02");

    }

    const completoInsertatNuevo = (e) => {
        console.log("el Json ", e);
        if (!e.error) {
            e.data.p_puesto = dataBaja.p_puesto;
            console.log("que pasa ")
            setNuevoVocal(e.data);
        }
    }

    const cancelar = e => {

    }

    const fetchEnviar = (data) => {
        console.log("se envia ", data)
        const archivos = data.archivos;
        axios.get(window.location.origin + '/JsonHelpers/jsonUrl.json').then((response) => {
            // console.log("urls::::::::::");
            console.log(response);
            localStorage.setItem('aplicaciones', response.data.urlLogin);
            let token = localStorage.getItem('accessToken');
            console.log('el token para insertar DomiciliosDeJunta: ', token);
            if (response.status === 200) {
                //setUrls(response.data);

                axios
                    .post(`${response.data.centralSesiones}/vocales/sustituir`, data, {
                        headers: {
                            Authorization: token,
                        },
                    })
                    .then((respuesta) => {
                        //localStorage.setItem("accessToken", response.data.token);
                        console.log(respuesta.data)
                        if (respuesta.data.code == 200) {
                            alertINE("success", "Seguardo con exito")
                            //generarFormImagenes(parseInt(respuesta.data.causa));
                        } else {
                            alertINE("eror", "no se puedo encontrar ese registro ")

                        }

                    })
                    .catch((erorr) => {
                        console.log('eroror ', erorr);
                    });
            }
        });

    }

    const generarFormImagenes = (idNuevo, archivos) => {

        const formData = new FormData();

        const listaNombres = ["0.png", "1.pdf", "2.png", "3.pdf"];
        var contentType = 'image/png';
        //console.log("ekl origin ", fotografiaDP.value.originFileObj)
        formData.append('files', archivos.value.originFileObj);
        formData.append('files', archivos.value.originFileObj);
        formData.append('files', archivos.value.originFileObj);

        formData.append('param', new Blob([JSON.stringify({
            idEstado: archivos.infoMenu.estadoSelec.idEstado,
            idDistrito: archivos.infoMenu.distritoFedSelec.idDistrito,
            modulo: "Vocales",
            idSubModulo: idNuevo,
            nombreSistema: "junta",
            modificar: true,
            nombreArchivo: listaNombres,
        })], { type: "application/json" }));

        console.log("el formData ", formData);
        guardaArchivo(formData).then((data) => {
            alertINE("success", "Seguardo con exito")
        }).catch((error) => {
            console.log("error guardar !", error)
        });
        return formData;
    }


    const guardaArchivo = (multipart) => {
        return new Promise((resolve, reject) => {
            axios.get(window.location.origin + "/JsonHelpers/jsonUrl.json").then((response) => {
                // console.log(response);
                if (response.status === 200) {
                    const url = response.data.serverGeneral + "/guardarArchivo";
                    let token = localStorage.getItem("accessToken");
                    console.log("el token para insertar DomiciliosDeJunta: ", token);

                    if (token !== undefined && token != null) {
                        console.log("paso a el token ", multipart);
                        axios
                            .post(url, multipart, {
                                headers: {
                                    Authorization: token,
                                },
                            })
                            .then((response) => {
                                // console.log("Exito sevice ", response);
                                localStorage.setItem("accessToken", response.data.token);
                                const temp = response.data;
                                resolve({ err: false, data: temp });
                            })
                            .catch((error) => {
                                console.log("Errror ! service  guardarARchivo", error)

                                resolve({
                                    err: true,
                                    mensaje: error,
                                });
                            });
                    }
                }
            });
        });
    }


    /**
     * tipo 1 sustitucion 
     * tipo 2 nuevo 
     * tipo 3 pendiente 
     * @param {*} e 
     */
    const enviar = e => {
        // falta evidencia 01 
        console.log("que valor tiene el vocal ide ", vocal)
        if (tipo == 1 && fechaBaja != null && motivo != null && dataBaja != null && vocal != null &&
            sustitutoFechaIngreso != null && sustitutoFechaNombramiento != null) {
            let data = {
                "idEstado": menu.infoMenu.estadoSelec.idEstado,
                "idDistrito": menu.infoMenu.distritoFedSelec.idDistrito,
                "idVocal": parseInt(id),
                "fechaIngreso": getFecha(dataBaja.p_fechaIngreso),
                "fechaNombremiento": getFecha(dataBaja.p_fechaNombramiento),
                "fechaBaja": fechaBaja,
                "idMotivo": motivo,
                "motivo": vocal.tipo_REGISTRO,
                "evidenciaPrimera": Evidencia01,
                "idSustituo": parseInt(vocal.id_VOCAL),
                "curp": vocal.curp,
                "fechaNuevoIngreso": sustitutoFechaIngreso,
                "fechaNombramientoNuevo": sustitutoFechaNombramiento,
                "evidenciaSegunda": Evidencia02,
                "status": tipo,
                "nuevoVocal": null,
            }
            fetchEnviar(data)
            alertINE("success", "Paso");
        } else {
            alertINE("eror", "Faltan datos");
        }
        if (tipo == 2 && fechaBaja != null && motivo != null && nuevoVocal != null) {

            let data = {
                "idEstado": menu.infoMenu.estadoSelec.idEstado,
                "idDistrito": menu.infoMenu.distritoFedSelec.idDistrito,
                "idVocal": parseInt(id),
                "fechaIngreso": getFecha(dataBaja.p_fechaIngreso),
                "fechaNombremiento": getFecha(dataBaja.p_fechaNombramiento),
                "fechaBaja": fechaBaja,
                "idMotivo": motivo,
                "motivo": null,
                "evidenciaPrimera": Evidencia01,
                "idSustituo": null,
                "curp": null,
                "fechaNuevoIngreso": sustitutoFechaIngreso,
                "fechaNombramientoNuevo": sustitutoFechaNombramiento,
                "evidenciaSegunda": null,
                "status": tipo,
                "nuevoVocal": nuevoVocal,
            }
            if (nuevoVocal != null) {

                fetchEnviar(data)
                alertINE("success", "Se va  a nuevo");
            }
            console.log("se va enviar nuevo ", data)
        }
        if (tipo == 3 && fechaBaja != null && motivo != null) {
            let data = {
                "idEstado": menu.infoMenu.estadoSelec.idEstado,
                "idDistrito": menu.infoMenu.distritoFedSelec.idDistrito,
                "idVocal": parseInt(id),
                "fechaIngreso": getFecha(dataBaja.p_fechaIngreso),
                "fechaNombremiento": getFecha(dataBaja.p_fechaNombramiento),
                "fechaBaja": fechaBaja,
                "idMotivo": motivo,
                "motivo": vocal != null ? vocal.tipo_REGISTRO : null,
                "evidenciaPrimera": Evidencia01,
                "idSustituo": null,
                "curp": null,
                "fechaNuevoIngreso": null,
                "fechaNombramientoNuevo": null,
                "evidenciaSegunda": null,
                "status": tipo,
                "nuevoVocal": null,
            }
            alertINE("success", "Se va  pendiente");
            fetchEnviar(data)
        }
        if (tipo == null) {
            alertINE("eror", "Faltan datos");
        }


    }
    const getFecha = (data) => {
        if (data != null) {
            let tempFechca = data.split(" ")[0].split("-");
            //let tempfecha= tempFechca.split("-");
            let fecha = tempFechca[2] + "/" + tempFechca[1] + "/" + tempFechca[0];
            return fecha;
        } else {
            return "N/A";
        }
        //return moment(new Date(data), "DD-MM-YYYY")
    }

    return (

        <div className="containerLista container" >
            <h1>Dar de baja a Vocal</h1>
            <p>Los datos con (<font color="#d5007f">*</font> ) son requeridos.</p>
            <br />
            {
                dataBaja != null ?

                    <div>
                        <h3>Datos generales</h3>
                        <div className=" col-md-12 col-sm-12">
                            <img className="img" src={urlImagen} alt="avatar" />
                            <label className="mostrar">VOCAL EJECUTIVA/O</label>
                        </div>

                        <div className="row" style={{ 'padding-top': '30px' }}>
                            <div className=" col-md-4 col-sm-12">
                                <label className="mostrar"> Nombre</label>
                                <br />
                                <p className="mostrar-varibles">{dataBaja.v_nombre}</p>
                            </div>
                            <div className=" col-md-4 col-sm-12">
                                <label> Fecha de ingreso</label>
                                <br />
                                <p className="mostrar-varibles">{getFecha(dataBaja.p_fechaIngreso)}</p>
                            </div>
                            <div className=" col-md-4 col-sm-6">
                                <label> Fecha de nombramiento</label>
                                <br />
                                <p className="mostrar-varibles">{getFecha(dataBaja.p_fechaNombramiento)}</p>
                            </div>

                            <div className=" col-md-4 col-sm-12">
                                <label> <font color="#d5007f">*</font> Fecha de baja</label>
                                <br />
                                <DatePicker placeHolder={"DD/MM/AAAA"} format={'DD/MM/YYYY'} onChange={(evento, fecha) => changeFechaBaja(evento, fecha)} />
                            </div>

                            <div className=" col-md-4 col-sm-12">
                                <label><font color="#d5007f">*</font> Motivo</label>
                                <br />
                                <Select onChange={changeMotivo} style={{ width: '100%' }}
                                    placeholder="Selecciona una opción">
                                    {
                                        motivos != null ?
                                            motivos.map((tem) => (
                                                <Select.Option value={tem.idMotivoBaja}>{tem.descripcion}</Select.Option>
                                            ))
                                            : null
                                    }
                                </Select>
                            </div>

                            <div className=" col-md-4 col-sm-6">
                                <label> Evidencia</label>
                                <br />
                                <Archivo getData={evidencia01} />
                            </div>
                            <div className=" col-md-6 col-sm-6">
                                <label><font color="#d5007f">*</font> Asignar cargo a otro vocal</label>
                                <br />
                                <Radio.Group onChange={changeAsignar} >
                                    <Radio value={true}>Si</Radio>
                                    <Radio value={false}>No</Radio>

                                </Radio.Group>
                            </div>

                            {asingar != null ?
                                asingar ?
                                    <div className="col-md-12">
                                        <div >
                                            <h1>Vocal sustituto</h1>
                                        </div>

                                        <div className="row">

                                            <div className="col-md-4 col-sm-12">
                                                <label> <font color="#d5007f">*</font> Nombre</label>
                                                <br />
                                                <Select onChange={changeNombre} style={{ width: '100%' }}
                                                    placeholder="Selecciona una opción">

                                                    <OptGroup label="Activos">
                                                        {
                                                            sustitutos != null ?
                                                                sustitutos.map((iter) => {
                                                                    if (iter.tipo_REGISTRO == "ACTIVO") {

                                                                        return (<Select.Option value={iter.id_VOCAL}>{iter.nombre}</Select.Option>)
                                                                    }
                                                                }
                                                                )
                                                                : null
                                                        }
                                                    </OptGroup>
                                                    <OptGroup label="Incactivos">
                                                        {
                                                            sustitutos != null ?
                                                                sustitutos.map((iter) => {
                                                                    if (iter.tipo_REGISTRO != "ACTIVO") {

                                                                        return (<Select.Option value={iter.id_VOCAL}>{iter.nombre}</Select.Option>)
                                                                    }
                                                                }
                                                                )
                                                                : null
                                                        }
                                                    </OptGroup>



                                                </Select>
                                            </div>
                                            <div className="col-md-4 col-sm-12">
                                                CURP
                                                    <br />
                                                {curp}
                                            </div>
                                            <div className="col-md-4 col-sm-12">
                                                Fecha de ingreso
                                       <br />
                                                {getFecha(dataBaja.p_fechaIngreso)}
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-md-4 col-sm-12">
                                                <label> <font color="#d5007f">*</font> Fecha de Ingreso (nuevo cargo)</label>
                                                <br />
                                                <DatePicker placeHolder={"DD/MM/AAAA"} format={'DD/MM/YYYY'} onChange={(evento, fecha) => changeSustituidoFechaIngreso(evento, fecha)} />
                                            </div>
                                            <div className="col-md-4 col-sm-12">
                                                <label> <font color="#d5007f">*</font> Fecha de nombramiento (nuevo cargo)</label>
                                                <br />
                                                <DatePicker placeHolder={"DD/MM/AAAA"} format={'DD/MM/YYYY'} onChange={(evento, fecha) => changeSustituidoFechaNombre(evento, fecha)} />
                                            </div>
                                            <div className="col-md-4 col-sm-12">
                                                <label>  Evidencia</label>
                                                <br />
                                                <Archivo getData={evidencia02} />
                                            </div>
                                        </div>
                                    </div>
                                    :
                                    <div className=" col-md-6 col-sm-6">
                                        <label><font color="#d5007f">*</font>  Asignar cargo a un nuevo vocal</label>
                                        <br />
                                        <Radio.Group onChange={changeAsignarNo} >
                                            <Radio value={true}>Si</Radio>
                                            <Radio value={false}>No</Radio>

                                        </Radio.Group>
                                    </div>
                                : null}

                            {tipo == 2 ? <Formulario menu={menu} estatus={false} completo={completoInsertatNuevo} idVocalBajaPuesto={dataBaja.p_puesto} /> : null}



                        </div>
                        <div className="row">
                            <div className="col-md-6 col-sm-12 ">
                                <Link to={`/vocales/home`}>

                                    <Button type="primary" onClick={cancelar}>
                                        cancelar
                                       </Button>
                                </Link>
                                <Button type="primary" onClick={enviar}>
                                    Enviar
                                       </Button>
                            </div>
                        </div>
                    </div>

                    :
                    null
            }
        </div>

    )
}

