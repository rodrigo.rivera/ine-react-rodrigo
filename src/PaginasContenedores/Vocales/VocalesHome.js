import React, { useState, useEffect } from "react";
import { BrowserRouter as Router, Route, Switch, Link } from "react-router-dom";
import { FileAddOutlined, FormOutlined } from '@ant-design/icons';
import { Table, Button, List, Popconfirm, Spin, Layout, Icon } from 'antd';

import Image from 'react-bootstrap/Image'
import axios from "axios";
//import Menu from "../Componets/Sider/Sider";

import jwtDecode from "jwt-decode";
import VocalesFormulario from "./VocalesFormulario";
const { Content } = Layout;
const { Column, ColumnGroup } = Table;
const antIcon = <Icon type="loading" style={{ fontSize: 30 }} spin />;
export default function HomeVocales(props) {

    // let dataBaja = [{
    //     nombre: "Rodrigo",
    //     cargo: "Rodrigo",
    //     movimiento: "Rodrigo",
    //     responsableMov: "Rodrigo",
    //     fechaHora: "Rodrigo",
    //     evidencia: "Rodrigo",
    // }]

    //localStorage.setItem("t", JSON.stringify(dataBaja));

    const [dataMenu] = useState(props.menu)

    const [lista, setLista] = useState(null)
    const [bitacora, setBitacora] = useState(null);
    let listaImagnes = [];

    const [urlImagen01, setUrlImagen01] = useState(null);
    const [urlImagen02, setUrlImagen02] = useState(null);
    const [urlImagen03, setUrlImagen03] = useState([]);
    const [urlImagen04, setUrlImagen04] = useState(null);
    const [urlImagen05, setUrlImagen05] = useState(null);
    const [urlImagen06, setUrlImagen06] = useState(null);
    const [listaImg, setListaImge] = useState([]);
    const columnas = [
        {
            title: "Nombre",
            dataIndex: "nombre",
            width: "15%"
        },
        {
            title: "Cargo",
            dataIndex: "cargo",
            width: "15%"

        }, {
            title: "Movimiento",
            dataIndex: "movimiento",
            width: "15%"
        },
        {
            title: "Responsable del Movimiento",
            dataIndex: "responsableMov",
            width: "15%"
        },
        {
            title: "Fecha",
            dataIndex: "fechaHora",
            width: "15%"
        },
        {
            title: "Accion",
            dataIndex: "evidencia",
            width: "15%",
            render: (text, record) =>
                bitacora.length >= 1 ? (
                    <>

                        <Link to={{
                            pathname: `/vocales/${record.idVocal}/mostrar`
                        }
                        } >
                            <Button
                                key={JSON.stringify(record.idVocal)}
                                shape="circle"
                                className=".ant-btn ant-btn-modifica"
                                style={{ marginLeft: '15px' }}
                            >
                                <FormOutlined className="iconMostrar" size={20} />
                            </Button>
                        </Link>



                    </>
                ) : null
        }
    ]
    const changeEdit = (id) => {
        console.log("el id es el numero ", id);
    }

    useEffect(() => {
        //console.log(dataMenu.infoMenu);

        if (dataMenu.infoMenu.distritoFedSelec != null && dataMenu.infoMenu.estadoSelec) {


            axios.get(window.location.origin + "/JsonHelpers/jsonUrl.json")
                .then((response) => {
                    // console.log("urls::::::::::");
                    //console.log(response);
                    localStorage.setItem('aplicaciones', response.data.urlLogin);
                    if (response.status === 200) {
                        //console.log("me voy a trarr ", localStorage.getItem("idDistrito"))
                        let data = {
                            //"idDetalleProceso": 106,
                            "idDistrito": dataMenu.infoMenu.distritoFedSelec.idDistrito,
                            "idEstado": dataMenu.infoMenu.estadoSelec.idEstado,
                        }
                        //console.log("los datos", data)

                        axios.post(`${response.data.centralSesiones}/vocales/consulta`, data).then((respuesta) => {
                            //localStorage.removeItem("idDistrito")


                            console.log("...................exito", dataMenu.infoMenu.distritoFedSelec.idDistrito)
                            setBitacora(respuesta.data.bitacora);
                            setLista(respuesta.data.vocalesHome);
                            // trae imagenes 
                            if (respuesta.data.vocalesHome != null) {
                                let i = 0;
                                respuesta.data.vocalesHome.forEach(temp => {
                                    // inicio 

                                    let data = {
                                        idEstado: dataMenu.infoMenu.estadoSelec.idEstado,
                                        idDistrito: dataMenu.infoMenu.distritoFedSelec.idDistrito,
                                        modulo: "Vocales",
                                        idSubModulo: temp.id,
                                        nombreSistema: "junta",
                                        modificar: true,
                                    }
                                    console.log("Mostrar datos ", data)
                                    let token = localStorage.getItem('accessToken');
                                    axios.post(`${response.data.centralSesiones}/vocales/descargaArchivo`, data, {
                                        headers: {
                                            Authorization: token,
                                        },
                                    })
                                        .then((respuesta) => {
                                            //localStorage.setItem("accessToken", response.data.token);
                                            //console.log("la data ", respuesta)
                                            let lista = base64ToBlobs(respuesta.data.archivo)
                                            //console.log(lista.blobs);
                                            let x = { id: temp.id, url: lista.blobs[0].source }
                                            console.log("las imagenes ", x);
                                            if (i === 0) {
                                                setUrlImagen01(x)
                                            }
                                            if (i === 1) {
                                                setUrlImagen02(x);
                                            }
                                            if (i === 2) {
                                                setUrlImagen03(x)
                                            }
                                            if (i === 3) {
                                                setUrlImagen04(x);
                                            }
                                            if (i === 4) {
                                                setUrlImagen04(x)
                                            }
                                            if (i === 5) {
                                                setUrlImagen05(x);
                                            }
                                            if (i === 6) {
                                                setUrlImagen06(x);
                                            }
                                            i++;
                                        })
                                        .catch((erorr) => {
                                            console.log('eroror ', erorr);
                                        });



                                    //fin 
                                });

                                for (let index = 0; index < 5; index++) {
                                    urlImagen03.push({ id: index });

                                }
                            }

                        }).catch((erorr) => {
                            console.log("eroror ", erorr)
                        });
                    }
                });

        }
        //setTest("valeria")

    }, []);



    const base64ToBlobs = (base64Images) => {
        //console.log("recibe ", base64Images)
        var images = { blobs: [] }

        for (var i = 0; i < base64Images.length; i++) {
            //console.log(base64Images[i].tipoArchivo)
            if (base64Images[i].archivoBase64 !== null) {
                images.blobs.push({
                    "source": b64toBlob(base64Images[i].archivoBase64, base64Images[i].tipoArchivo),
                    "name": base64Images[i].nombreArchivo.split('-')[0],
                    "order": base64Images[i].nombreArchivo.split('-')[1]
                });
            }

        }
        //console.log("Archivos:::::::::::::::::::::::")
        //console.log(images)
        return images
    };

    function b64toBlob(b64Data, contentType) {
        contentType = contentType || '';
        var sliceSize = 512;
        b64Data = b64Data.replace(/^[^,]+,/, '');
        b64Data = b64Data.replace(/\s/g, '');
        var byteCharacters = window.atob(b64Data);
        var byteArrays = [];

        for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            var slice = byteCharacters.slice(offset, offset + sliceSize);

            var byteNumbers = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }

            var byteArray = new Uint8Array(byteNumbers);

            byteArrays.push(byteArray);
        }

        var blob = URL.createObjectURL(new Blob(byteArrays, { type: contentType }));
        return blob;
    }

    const buscarImg = id => {
        //console.log("el valor de el id", id)
        if (urlImagen01 != null) {
            //console.log("tamaño ", urlImagen01)
            console.log("tamaño ", urlImagen01.id, "==", id)

            if (urlImagen01.id == id) {
                return urlImagen01.url;
            }

            if (urlImagen02 != null && urlImagen02.id == id) {
                return urlImagen02.url;
            }
            if (urlImagen03 != null && urlImagen03.id == id) {
                return urlImagen03.url;
            }
            if (urlImagen04 != null && urlImagen04.id == id) {
                return urlImagen04.url;
            }
            if (urlImagen05 != null && urlImagen05.id == id) {
                return urlImagen05.url;
            }
            if (urlImagen06 != null && urlImagen06.id == id) {
                return urlImagen06.url;
            }
        }

        return "https://www.w3schools.com/css/paris.jpg";
    }

    return (

        <>

            <br />
            
            {lista != null ?
                <>
                    <h1 style={{
                        'paddingLeft': '12%',
                        'paddingRight': '12%',

                    }}>Vocales</h1>
                    <div className="row" style={{
                        'paddingLeft': '12%',
                        'paddingRight': '12%',

                        'top': '60px'
                    }}>
                        <br />
                        <Link to="/vocales/crear">
                            <br />
                            <Button className="btn btn-sq-lg btn-secondaryINE">
                                <FileAddOutlined style={{ fontSize: '64px', color: 'black' }} /><br />
                        Nuevo registro

                        </Button>
                        </Link>
                    </div>

                    <div className="row" style={{

                        'paddingTop': '60px',
                        'paddingLeft': '10%',

                        'paddingRight': '10%',

                    }}>
                        <List style={{ background: "#fff" }}
                            itemLayout="horizontal"
                            dataSource={lista}
                            renderItem={item => (

                                <List.Item>
                                    {/* {console.log("---",item)} */}
                                    <List.Item.Meta
                                        avatar={<Image
                                            width={80}
                                            height={80}
                                            alt="logo"
                                            src={buscarImg(item.id)}
                                            roundedCircle={true}
                                        />}
                                        title={<p >{item.cargo}</p>}

                                        description={<p >{item.nombre}</p>}
                                    />
                                    <div>
                                        {item.mostrar ?
                                            <Link to={{
                                                pathname: `${item.id}/mostrar`
                                            }
                                            } >
                                                {/* <i className='.ant-btn ant-btn-modifica' style={{ 'margin-right': '9px' }}>
                                                            <FormOutlined style={{ marginLeft: '9px' }} size={20} />
                                                            

                                                        </i> */}
                                                <Button
                                                    key={JSON.stringify(item.id)}
                                                    shape="circle"
                                                    className=".ant-btn ant-btn-modifica"
                                                    style={{ marginLeft: '15px' }}
                                                >
                                                    <FormOutlined className="iconMostrar" size={20} />
                                                </Button>
                                            </Link>
                                            : null}

                                        {item.editar ?
                                            <Link to={{
                                                pathname: `/vocales/${item.id}/editar`,
                                                query: { n: "rodrigo", f: [{ a: 2 }, { b: 2 }] }
                                            }
                                            } >
                                                {/* <i className='.ant-btn ant-btn-modifica' style={{ 'margin-right': '15px' }}>
                                                            <FormOutlined style={{ marginLeft: '15px' }} size={20} />
                                                             {console.log("que es item",item)} }

                                                        </i> */}
                                                <Button
                                                    key={JSON.stringify(item.id)}
                                                    shape="circle"
                                                    className=".ant-btn ant-btn-modifica"
                                                    style={{ marginLeft: '15px' }}
                                                >
                                                    <FormOutlined className="iconEditarHome" size={20} />
                                                </Button>
                                            </Link>

                                            : null
                                        }
                                        {item.remplazar ?
                                            <Link to={{
                                                pathname: `/vocales/${item.id}/remplazar`
                                            }
                                            } >
                                                <Button
                                                    key={JSON.stringify(item.id)}
                                                    shape="circle"
                                                    className=".ant-btn ant-btn-modifica"
                                                    style={{ marginLeft: '15px' }}
                                                >
                                                    <FormOutlined className="iconSutituye" size={20} />
                                                </Button>
                                            </Link>
                                            : null
                                        }

                                        {item.incompleto ?
                                            <Link to={{
                                                pathname: `/vocales/${item.id}/remplazar`
                                            }
                                            } >
                                                <Button
                                                    key={JSON.stringify(item.id)}
                                                    shape="circle"
                                                    className=".ant-btn iconSutituye"
                                                    style={{ marginLeft: '15px' }}
                                                >
                                                    <FormOutlined size={20} />
                                                </Button>
                                            </Link>
                                            : null
                                        }


                                    </div>
                                </List.Item>

                            )}
                        />
                    </div>
                    <br />
                    <br />

                    <div className="row" style={{



                        'top': '60px'
                    }}>
                        <h6>Bitacora</h6>
                        <br />


                        {
                            bitacora != null ?
                                <Table dataSource={bitacora} columns={columnas} />
                                :
                                null
                        }


                    </div>

                </>

                : null
                // <div className="containerLista container" >
                //     <div className="loading-style">
                //         <Spin indicator={antIcon} tip="Cargando..."></Spin>
                //     </div>
                // </div>
            }
        </>
    );
}
