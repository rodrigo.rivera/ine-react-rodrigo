import React, { useState, useEffect } from "react";
import 'antd/dist/antd.css';
import { Table, Tag, Avatar, Spin, Icon } from "antd";
import axios from "axios";
import { useParams, Link } from 'react-router-dom';
import Estados from "../../Assets/files/Estados.json";
import moment from "moment";
const { Column, ColumnGroup } = Table;
const antIcon = <Icon type="loading" style={{ fontSize: 30 }} spin />;
export default function VocalesMostrar(props) {
    let { id } = useParams();

    //<console.log(props)

    const [dataMenu] = useState(props.menu)
    const [data, setData] = useState(null);
    const [experiencia, setExperiencia] = useState(null);
    const [telefonos, setTefonos] = useState(null);
    const [urlImagen, setUrlImagen] = useState(null);
    const [urlIFirma, setUrlIFirma] = useState(null);
    const [urlINombramiento, setUrlINombramiento] = useState(null);



    useEffect(() => {
        console.log("la data", dataMenu, "   ", dataMenu.infoMenu.estadoSelec != null)
        if (dataMenu != null && dataMenu.infoMenu.estadoSelec != null) {

            axios.get(window.location.origin + "/JsonHelpers/jsonUrl.json")
                .then((response) => {
                    // console.log("urls::::::::::");
                    console.log(response);
                    let token = localStorage.getItem('accessToken');
                    if (response.status === 200) {
                        //setUrls(response.data);
                        let datos = {
                            //"idDetalleProceso": 106,
                            "idEstado": localStorage.getItem("idEstado"),
                            "idDistrito": localStorage.getItem("idDistrito"),
                            "idVocal": id
                        }
                        console.log("la data", datos)

                        // imagenes 

                        let data = {
                            idEstado:  dataMenu.infoMenu.estadoSelec.idEstado,
                            idDistrito:  dataMenu.infoMenu.distritoFedSelec.idDistrito,
                            modulo: "Vocales",
                            idSubModulo: id,
                            nombreSistema: "junta",
                            modificar: true,
                        }
                        console.log("Mostrar datos ",data)
                        axios.post(`${response.data.centralSesiones}/vocales/descargaArchivo`, data, {
                            headers: {
                                Authorization: token,
                            },
                        })
                            .then((respuesta) => {
                                //localStorage.setItem("accessToken", response.data.token);
                                console.log("la data ", respuesta)
                                let lista =base64ToBlobs(respuesta.data.archivo)
                                console.log(lista.blobs);
                                setUrlImagen(lista.blobs[0].source);
                                setUrlINombramiento(lista.blobs[1].source);
                                setUrlIFirma(lista.blobs[2].source);

                            })
                            .catch((erorr) => {
                                console.log('eroror ', erorr);
                            });


                        //datos 
                        axios.post(`${response.data.centralSesiones}/vocales/getVocal`, datos).then((respuesta) => {

                            console.log(respuesta.data)
                            console.log(respuesta.data.vocal)
                            if (respuesta.data.code == 200) {

                                const temp = respuesta.data.vocal.experiencia;
                                let listaExperiencia = [];
                                temp.forEach(b => {

                                    let template = {
                                        fechaIncio: getFecha(b.fechaIncio),
                                        fechaTermino: getFecha(b.fechaTermino),
                                        rama: b.rama == 1 ? "Administrativo" : "Servicio Profesional",
                                        puesto: getPuesto(b.puesto),
                                        instituto: b.instituto ? "INE" : "OPL",
                                        entidad: getEntidad(b.entidad),
                                        distrito: getDistrito(b.entidad, b.distrito)
                                    }
                                    //console.log(template);
                                    listaExperiencia.push(template);
                                });
                                const telefonos = getTelefonos(respuesta.data.vocal.telefonos);
                                setTefonos(telefonos)
                                setData(respuesta.data.vocal);
                                setExperiencia(listaExperiencia)
                                setData(respuesta.data.vocal);
                            } else {
                                setData(null);
                            }
                        }).catch((erorr) => {
                            console.log("eroror ", erorr)
                        });
                    }
                });

        }

        //setTest("valeria")

    }, []);


    const columnasExperiencia = [

        {
            title: "Fecha inicio",
            dataIndex: "fechaIncio",
            width: "14%"
        },
        {
            title: "Fecha término",
            dataIndex: "fechaTermino",
            width: "14%"
        },
        {
            title: "Rama",
            dataIndex: "rama",
            width: "14%"
        },
        {
            title: "Puesto",
            dataIndex: "puesto",
            width: "14%"
        },
        {
            title: "Instituto",
            dataIndex: "instituto",
            width: "14%"
        },
        {
            title: "Entidad",
            dataIndex: "entidad",
            width: "14%"
        },
        {
            title: "Distrito",
            dataIndex: "distrito",
            width: "14%"
        },

    ];

    const getCiudadania = (data) => {

        if (data == 1) {
            return "Mexicana/o";
        }
        if (data == 2) {
            return "Extrangera/o";
        }
        if (data == 3) {
            return "Nacionalizado/a";
        }

    }
    const getPuesto = (id) => {
        //console.log("puesto ", id)

        if (id != null) {
            return JSON.parse(localStorage.getItem("puestos"))[id - 1].descripcion

        } else {
            return "N/A";
        }

    }

    const getTelefonos = (listaTelefono) => {

        return (
            <>
                {listaTelefono != null ?
                    listaTelefono.map(tem =>
                        <>
                            {tem.tipo == 1 ? "Móvil" : "Casa"} {tem.telefono}
                            <br />
                        </>
                    )
                    :
                    null
                }
            </>)
    }

    const getFecha = (data) => {
        if (data != null) {
            let tempFechca = data.split(" ")[0].split("-");
            //let tempfecha= tempFechca.split("-");
            let fecha = tempFechca[2] + "/" + tempFechca[1] + "/" + tempFechca[0];
            return fecha;
        } else {
            return "N/A";
        }
        //return moment(new Date(data), "DD-MM-YYYY")
    }

    const getProfesion = (id) => {
        //console.log("la lista ", listaProfesiones)
        const profesiones = JSON.parse(localStorage.getItem('profesiones'));
        return profesiones[id - 1].descripcion;

    };
    const getEntidad = (id) => {
        // console.log("entidad   ",id)
        // console.log(Estados.estados[id]);
        //console.log("El", id)
        if (id != null) {
            return Estados.estados[id - 1].nombreEstado;

        } else {
            return "Entidad invalida";
        }
    }

    const getDistrito = (idEstado, idDistrito) => {
        //console.log(Estados.estados[idEstado].distritos[idDistrito].nombreDistrito);

        if (idEstado != null && idDistrito != null) {
            //console.log("idEstado: ", idEstado, " distri: ", idDistrito, "==", Estados.estados[idEstado-1].distritos[idDistrito-1])

            if (Estados.estados[idEstado - 1].distritos[idDistrito - 1] == undefined) {
                return "distrito invalido"
            } else {
                return Estados.estados[idEstado - 1].distritos[idDistrito - 1].nombreDistrito;

            }
        } else {
            return "distrito invalido"
        }
    }

    const getTratamiento = (id) => {
        //console.log("la lista ", listaTratamiento)
        const tramientoList = JSON.parse(localStorage.getItem('tramiento'));

        return tramientoList[id - 1].descripcion;
    };

    const base64ToBlobs = (base64Images) => {
        console.log("recibe ", base64Images)
        var images = { blobs: [] }

        for (var i = 0; i < base64Images.length; i++) {
            //console.log(base64Images[i].tipoArchivo)
            if (base64Images[i].archivoBase64 !== null) {
                images.blobs.push({
                    "source": b64toBlob(base64Images[i].archivoBase64, base64Images[i].tipoArchivo),
                    "name": base64Images[i].nombreArchivo.split('-')[0],
                    "order": base64Images[i].nombreArchivo.split('-')[1]
                });
            }

        }
        console.log("Archivos:::::::::::::::::::::::")
        console.log(images)
        return images
    };

    function b64toBlob(b64Data, contentType) {
        contentType = contentType || '';
        var sliceSize = 512;
        b64Data = b64Data.replace(/^[^,]+,/, '');
        b64Data = b64Data.replace(/\s/g, '');
        var byteCharacters = window.atob(b64Data);
        var byteArrays = [];

        for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            var slice = byteCharacters.slice(offset, offset + sliceSize);

            var byteNumbers = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }

            var byteArray = new Uint8Array(byteNumbers);

            byteArrays.push(byteArray);
        }

        var blob = URL.createObjectURL(new Blob(byteArrays, { type: contentType }));
        return blob;
    }


    return (
        <div  >
            <Link to={{
                pathname: `/vocales/home`
            }
            } >

                <u className="letras" ><i className="iconAtras" />Regresar </u>
            </Link>
            <br />
            <br />
            <h1>Consulta de Vocal</h1>
            <br />
            {
                data != null ?
                    <>
                        <h3 style={{ 'color': '#e149a4' }}>Datos Personales</h3>
                        <img src={urlImagen}
                            alt="avatar"
                            style={{ width: '120px', height: '120px', 'Envolvente': '150px', 'rounded': '3px', 'border': '1px', 'padding-bottom': '30px' }} />
                        <label className="mostrar">{getPuesto(data.p_puesto)}</label>

                        <div className="row">
                            <div className="col-md-4 col-sm-12">
                                <label className="mostrar">Nombre</label>
                                <br />
                                <p className="mostrar-varibles">{data.v_nombre}</p>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-4 col-sm-12">
                                <label className="mostrar">Clave de elector</label>
                                <br />
                                <p className="mostrar-varibles">{data.v_claveElector}</p>
                            </div>
                            <div className="col-md-4 col-sm-6">
                                <label className="mostrar">CURP</label>
                                <br />
                                <p className="mostrar-varibles">{data.v_curp}</p>
                            </div>
                            <div className="col-md-4 col-sm-6">
                                <label className="mostrar">Género</label>
                                <br />
                                <p className="mostrar-varibles">{data.v_genero == 'H' ? "Hombre" : "Mujer"}</p>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-4 col-sm-12">
                                <label className="mostrar">Fecha de nacimiento</label>
                                <br />
                                <p className="mostrar-varibles">{getFecha(data.v_fechaNacimiento)}</p>
                            </div>
                            <div className="col-md-4 col-sm-12">
                                <label className="mostrar">Ciudadana/o mexicana/o</label>
                                <br />
                                <p className="mostrar-varibles">{getCiudadania(data.v_ciudadania)}</p>

                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-4 col-sm-12">
                                <label className="mostrar">Entidad Federativa de nacimiento</label>
                                <br />
                                <p className="mostrar-varibles">{getEntidad(data.v_entidad)}</p>
                            </div>
                            <div className="col-md-4 col-sm-12">
                                <label className="mostrar">Municipio de nacimiento</label>
                                <br />
                                <p className="mostrar-varibles">{getDistrito(data.v_entidad, data.v_municipio)}</p>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-4 col-sm-12">
                                <label className="mostrar">Profesión</label>
                                <br />
                                <p className="mostrar-varibles">{getProfesion(data.v_profesion)}</p>
                            </div>
                            <div className="col-md-4 col-sm-12">
                                <label className="mostrar">Tratamiento</label>
                                <br />
                                <p className="mostrar-varibles">{getTratamiento(data.v_tratamiento)}</p>
                            </div>
                            <div className="col-md-4 col-sm-12">
                                <label className="mostrar">Puesto que ejerce</label>
                                <br />
                                <p className="mostrar-varibles">{data.puesto}</p>
                            </div>
                        </div>
                        <br />
                        <div className="row">
                            <h3 style={{
                                'color': '#e149a4'
                            }}>Contacto</h3>
                        </div>
                        <div className="row">
                            <div className="col-md-4 col-sm-12">
                                <label className="mostrar">Calle y número</label>
                                <br />
                                <p className="mostrar-varibles">{data.d_calleContacto + "  " + data.d_numeroExterior}</p>
                            </div>
                            <div className="col-md-4 col-sm-12">
                                <label className="mostrar">Código Postal</label>
                                <br />
                                <p className="mostrar-varibles">{data.d_codigoPostalContacto}</p>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-4 col-sm-12">
                                <label className="mostrar">Entidad Federativa</label>
                                <br />
                                <p className="mostrar-varibles">{data.d_entidadContacto}</p>
                            </div>
                            <div className="col-md-4 col-sm-12">
                                <label className="mostrar">Municipio</label>
                                <br />
                                <p className="mostrar-varibles">{data.d_municipioContacto}</p>
                            </div>
                            <div className="col-md-4 col-sm-12">
                                <label className="mostrar">Colonia o Localidad</label>
                                <br />
                                <p className="mostrar-varibles">{data.d_coloniaContacto}</p>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-4 col-sm-12">
                                <label className="mostrar">Correo electrónico</label>
                                <br />
                                <p className="mostrar-varibles">{data.v_correoContacto}</p>
                            </div>
                            <div className="col-md-4 col-sm-12">
                                <label className="mostrar">Teléfono</label>
                                <br />
                                <p className="mostrar-varibles">{telefonos}</p>
                            </div>
                        </div>
                        <br />
                        <div className="row">
                            <h3 style={{
                                'color': '#e149a4'
                            }}>Estatus</h3>
                        </div>
                        <div className="row">
                            <div className="col-md-4 col-sm-12">
                                <label className="mostrar">Fecha de ingreso</label>
                                <br />
                                <p className="mostrar-varibles">{getFecha(data.p_fechaIngreso)}</p>
                            </div>
                            <div className="col-md-4 col-sm-12">
                                <label className="mostrar">Fecha de nombramiento</label>
                                <br />
                                <p className="mostrar-varibles">{getFecha(data.p_fechaNombramiento)}</p>
                            </div>
                            <div className="col-md-4 col-sm-12">
                                <label className="mostrar">Fecha de titularidad</label>
                                <br />
                                <p className="mostrar-varibles">{getFecha(data.p_fechaTitulariad)}</p>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-4 col-sm-12">
                                <label className="mostrar">Nombramiento</label>
                                <br />
                                <a href={urlINombramiento} download  >   <i className="iconClip" /><a className ="link">Nombramiento.pdf</a> </a> 
                            </div>
                            <div className="col-md-4 col-sm-12">
                                <label className="mostrar">Puesto</label>
                                <br />
                                <p className="mostrar-varibles">{data.p_tipoPuesto == 1 ? "Propietario" : "Encargado del Despacho"}</p>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-4 col-sm-12">
                                <label className="mostrar">Captura firma</label>
                                <br />
                                <img src={urlIFirma}
                                    alt="avatar"
                                    style={{ width: '120px', height: '120px', 'Envolvente': '150px', 'rounded': '3px', 'border': '1px', 'padding-bottom': '30px' }} />
                            </div>
                        </div>
                        <br />
                        <div className="row">
                            <h3 style={{
                                'color': '#e149a4', 'padding-top': '20px',
                                'padding-bottom': '20px'
                            }}>Experiencia</h3>
                        </div>
                        <div>
                            <Table dataSource={experiencia} pagination={false} columns={columnasExperiencia} />
                        </div>

                        <div className="row">
                            <h3 style={{
                                'color': '#e149a4', 'padding-top': '20px',
                                'padding-bottom': '20px'
                            }}>Movimientos registrados</h3>
                        </div>
                        <div>
                            <div style={{}} >
                                <Table dataSource={data.bitacora} theme="dark" pagination={false}>
                                    <Column title="Movimiento" dataIndex="movimiento" key="movimiento" />
                                    <Column title="Fecha" dataIndex="fecha" key="fecha" />
                                    <Column title="Vocal sustituto" dataIndex="vocal" key="vocal" />
                                    <Column title={<p>Fecha de movimiento<br />(nuevo cargo)</p>} dataIndex="fechaIngreso" key="fechaIngreso" />
                                    <Column title={<p>Fecha de nombramiento<br />(nuevo cargo)</p>} dataIndex="fechaNombramiento" key="fechaNombramiento" />
                                    <Column title="Evidencia" dataIndex="evidencia" key="evidencia" />
                                </Table>
                            </div>
                        </div>
                    </>

                    : <div className="loading-style">
                        <Spin indicator={antIcon} tip="Cargando..."></Spin>
                    </div>
            }


        </div>


    )

}