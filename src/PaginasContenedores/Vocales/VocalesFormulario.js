import React, { useState, useEffect } from "react";

import { Table, Tooltip, Steps, Button, Select, Radio, DatePicker, Checkbox } from 'antd';
import InputINE from '../../Components/Input/inputINE';
import CargarImagen from "../../Components/Imagenes/CargarImagen"
import CargarArchivo from "../../Components/Imagenes/cargarArchivos"
import { getCodigoPolstal } from "../../Components/CodigoPostal/CodigoPostal";
import { TablaRegistro } from "../../Components/Tablas/VocalFormulario";
import Telefonos from "../../Components/Telefonos/telefonos";
import { useParams, Link } from 'react-router-dom';
import Experiencia from "../../Components/Experiencia/VocalesExperiecia";
import '../../Assets/css/vocales.css';
import moment from "moment";
import Estados from "../../Assets/files/Estados.json";
import axios from 'axios';
import alertINE from "../../Components/Alert/AlertsINE";
import { Dialog, DialogContent, DialogActions } from "@material-ui/core";
//import { UploadOutlined } from '@ant-design/icons';


const { Column } = Table;
const { Step } = Steps;
const { Option } = Select;



export default function VocalesFormulario(props) {

    const { estatus, completo, idVocalBajaPuesto, history } = props;
    const [alertOpen, setAlertOpen] = useState(false);
    const [editarOpen, setOpenEditar] = useState(null);
    const [mensajeOpenAlert, setMensjeOpenAler] = useState(null);
    const [idVocalCurp, setIdVocalCurp] = useState(-1);

    const [dataMenu] = useState(props.menu)
    const [tempProfesiones, setTempProfesiones] = useState(null);
    const [tempTratamiento, setTempTratamiento] = useState(null);
    const [tempCargo, setCargo] = useState(null);
    const [cargoSustituir] = useState([{ id: 1, descripcion: "VOCAL EJECUTIVA/O" }, { id: 2, descripcion: "VOCAL SECRETARIA/O" }, { id: 3, descripcion: "VOCAL DE ORGANIZACIÓN ELECTORAL" }, { id: 4, descripcion: "VOCAL DEL REGISTRO FEDERAL DE ELECTORES" }, { id: 5, descripcion: "VOCAL DE CAPACITACIÓN ELECTORAL Y EDUCACIÓN CÍVICA" }, { id: 6, descripcion: "ENCARGADA/O DEL DESPACHO DEL VE" }]);
    const [listaPuestos, setListaPuestos] = useState(null);
    const [editar] = useState(false);

    useEffect(() => {
        //console.log(dataMenu.infoMenu);
        if (dataMenu.infoMenu.distritoFedSelec != null && dataMenu.infoMenu.estadoSelec) {
            //console.log("paso!")

            axios.get(window.location.origin + '/JsonHelpers/jsonUrl.json').then((response) => {
                // console.log("urls::::::::::");
                console.log(response);
                localStorage.setItem('aplicaciones', response.data.urlLogin);
                let token = localStorage.getItem('accessToken');
                console.log('el token para insertar DomiciliosDeJunta: ', token);
                if (response.status === 200) {
                    //setUrls(response.data);

                    axios
                        .post(`${response.data.centralSesiones}/vocales/consulta`, {
                            idDistrito: localStorage.getItem("idDistrito"),
                            idEstado: localStorage.getItem("idEstado")
                        }, {
                            headers: {
                                Authorization: token,
                            },
                        })
                        .then((respuesta) => {
                            //localStorage.setItem("accessToken", response.data.token);

                            console.log(respuesta.data)
                            //console.log(respuesta.data.vocalesHome)
                            if (respuesta.data.vocalesHome.length >= 0 && respuesta.data.vocalesHome.length < 6) {
                                setCargo(getCargos(respuesta.data.vocalesHome));
                                //console.log("paso")
                            } else {
                                setCargo([
                                    {
                                        id: 1,
                                        descripcion: "VOCAL EJECUTIVA/O"
                                    },
                                    {
                                        id: 2,
                                        descripcion: "VOCAL SECRETARIA/O"
                                    },
                                    {
                                        id: 3,
                                        descripcion: "VOCAL DE ORGANIZACIÓN ELECTORAL"
                                    },
                                    {
                                        id: 4,
                                        descripcion: "VOCAL DEL REGISTRO FEDERAL DE ELECTORES"
                                    },
                                    {
                                        id: 5,
                                        descripcion: "VOCAL DE CAPACITACIÓN ELECTORAL Y EDUCACIÓN CÍVICA"
                                    },
                                    {
                                        id: 6,
                                        descripcion: "ENCARGADA/O DEL DESPACHO DEL VE"
                                    }])
                            }
                            setTempTratamiento(respuesta.data.tratamiento);
                            setTempProfesiones(respuesta.data.profesiones)
                            setListaPuestos(respuesta.data.puestos);

                        })
                        .catch((erorr) => {
                            console.log('eroror ', erorr);
                        });
                }
            });


        }
        //setTest("valeria")

    }, []);




    const getCargos = (data) => {

        let lista = [
            {
                id: 1,
                descripcion: "VOCAL EJECUTIVA/O"
            },
            {
                id: 2,
                descripcion: "VOCAL SECRETARIA/O"
            },
            {
                id: 3,
                descripcion: "VOCAL DE ORGANIZACIÓN ELECTORAL"
            },
            {
                id: 4,
                descripcion: "VOCAL DEL REGISTRO FEDERAL DE ELECTORES"
            },
            {
                id: 5,
                descripcion: "VOCAL DE CAPACITACIÓN ELECTORAL Y EDUCACIÓN CÍVICA"
            },
            {
                id: 6,
                descripcion: "ENCARGADA/O DEL DESPACHO DEL VE"
            }]

        let temp = [];
        data.forEach(i => {
            temp.push(i.idCargo);
        })
        let faltan = [];
        lista.forEach(i => {
            let t = temp.indexOf(i.id);

            if (t == -1) {
                faltan.push(i);
            }
        });
        console.log(faltan)
        return faltan;
    }


    const [numeroPagina, setnumeroPagina] = useState({ pagina: 0 });
    /**
     * pagina 01
     */
    const [fotografiaDP, setFotografiaDP] = useState({ estatus: false, value: null })
    const [cargoDP, setCargoDP] = useState({ estatus: false, value: null })
    const [apellidoMaternoDP, setApellidoMaternoDP] = useState({ estatus: false, value: null });
    const [apellidoPaternoDP, setApellidoPaternoDP] = useState({ estatus: false, value: null });
    const [nombreDP, setNombreDP] = useState({ estatus: false, value: null });
    const [claveElectorDP, setClaveElectorDP] = useState({ estatus: false, value: null });
    const [curpDP, setCurpDP] = useState({ estatus: false, value: null });
    const [generoDP, setGeneroDP] = useState({ estatus: false, value: null });
    const [fechaNacimientoDP, setFechaNacimientoDP] = useState({ estatus: false, value: null });
    const [ciudadanaDP, setCiudadanaDP] = useState({ estatus: false, value: null });
    const [entidadDP, setEntidadDP] = useState({ estatus: false, value: null });
    const [municipioDP, setMunicipioDP] = useState({ estatus: false, value: null });
    const [profesionDP, setProfesionDP] = useState({ estatus: false, value: null });
    const [tratamientoDP, setTratamientoDP] = useState({ estatus: false, value: null });
    const [puestoDP, setPuestoDP] = useState({ estatus: false, value: null });
    //auxiliares
    const [llenarDistrito, setLlenarDistrito] = useState(null);
    /**
     * pagina 02
     */
    const [calleC, setCalleC] = useState({ estatus: false, value: null });
    const [numeroExteriorC, setNumeroExteriorC] = useState({ estatus: false, value: null });
    const [numeroInteriorC, setNumeroInteriorC] = useState({ estatus: false, value: null });
    const [sinNumeroC, setSinNumeroC] = useState({ estatus: false, value: false });
    const [codigoPostalC, setCodigoPostalC] = useState({ estatus: false, value: null });
    const [entidadFederativaC, setEntidadFederativaC] = useState({ estatus: false, value: null });
    const [municipioC, setMunicipioC] = useState({ estatus: false, value: null });

    //auxiliares
    const [apiCodigo, setApiCodigo] = useState(false);
    const [colonias, setColonias] = useState(null)
    const [coloniaC, setColoniaC] = useState({ estatus: false, value: null });
    const [correoC, setCorreoC] = useState({ estatus: false, value: null });
    const [telefonoC, setTelefonoC] = useState({ estatus: false, value: null });
    /**
     * pagina 03
     */
    const [fechaIngresoEs, setFechaIngresoEs] = useState({ estatus: false, value: null });
    const [fechaNombramientoEs, setFechaNombramientoEs] = useState({ estatus: false, value: null });
    const [fechaTitularidadEs, setFechaTitularidadEs] = useState({ estatus: false, value: null });
    const [nombramientoEs, setNombramientoEs] = useState({ estatus: false, value: null });
    const [puestoEs, setPuestoEs] = useState({ estatus: false, value: null })
    const [firmaEs, setFirmaEs] = useState({ estatus: false, value: null })
    /**
     * pagina 04
     */
    const [experiencia, setExperiencia] = useState({ estatus: false, value: null })
    const [experienciaSend, setExperienciaSend] = useState({ estatus: false, value: null })

    const [guardar, setGuardar] = useState(false);

    /**
     * pagina 02
     */

    const [checkBoxNum, setCheckBoxNum] = useState(null)

    const changeTelefono = (data) => {
        console.log("telefonos fomrulario", data)
        setTelefonoC({ estatus: true, value: data })

    }

    /**
     * 
     */
    const changeExperiencia = (data) => {
        console.log("experiencia fomrulario", data)
        setExperiencia({ estatus: true, value: data })

    }

    const changeExperienciaSend = (data) => {
        //console.log("EXperiencias send +", data)
        setExperienciaSend({ estatus: true, value: data })

    }
    /**
     * metodos botones
     */

    const next = () => {
        setnumeroPagina({ pagina: numeroPagina.pagina + 1 });

    }

    const prev = () => {
        setnumeroPagina({ pagina: numeroPagina.pagina - 1 });
    }

    const enviar = () => {
        console.log("Expericnia ", experienciaSend)
        console.log(
            "nombreDP: ", nombreDP.estatus, "\n",
            "claveElectorDP: ", claveElectorDP.estatus, "\n",
            "curpDP: ", curpDP.estatus, "\n",
            "generoDP: ", generoDP.estatus, "\n",
            "fechaNacimientoDP: ", fechaNacimientoDP.estatus, "\n",
            "ciudadanaDP: ", ciudadanaDP.estatus, "\n",
            "entidadDP: ", entidadDP.estatus, "\n",
            "municipioDP: ", municipioDP.estatus, "\n",
            "calleC: ", calleC.estatus, "\n",
            "codigoPostalC: ", codigoPostalC.estatus, "\n",
            "entidadFederativaC: ", entidadFederativaC.estatus, "\n",
            "municipioC: ", municipioC.estatus, "\n",
            "coloniaC: ", coloniaC.estatus, "\n",
            "correoC: ", correoC.estatus, "\n",
            "fechaIngresoEs: ", fechaIngresoEs.estatus, "\n",
            "puestoEs: ", puestoEs.estatus, "\n",

            "telefonoC:", (telefonoC.value.length > 0), "\n",
            "experienciaSend:", (experienciaSend.value.length > 0), "\n",
            "apellido:", (apellidoMaternoDP.estatus || apellidoPaternoDP.estatus), "\n",

        )

        let data = {
            /**
             * pagina 01
             */
            //puestoDP.value  // falta este dato
            "v_id": null,
            "usuario": null,
            "v_nombre": nombreDP.value,
            "v_apellidoPaterno": apellidoPaternoDP.value,
            "v_apellidoMaterno": apellidoMaternoDP.value,
            "v_claveElector": claveElectorDP.value,
            "v_curp": curpDP.value,
            "v_genero": generoDP.value,
            "v_fechaNacimiento": fechaNacimientoDP.value,
            "v_ciudadania": ciudadanaDP.value,
            "v_entidad": entidadDP.value,
            "v_municipio": municipioDP.value,
            "v_profesion": profesionDP.value,
            "v_tratamiento": tratamientoDP.value,
            "v_img": "fotografiaDP.value",
            "v_correoContacto": correoC.value,
            /**
             * pagina 02
             */

            "d_calleContacto": calleC.value,
            "d_numeroExterior": numeroExteriorC.value,
            "d_numeroInterior": numeroInteriorC.value,
            "d_coloniaContacto": coloniaC.value,
            "d_idMunicipioContacto": -1,
            "d_municipioContacto": municipioC.value,
            "d_codigoPostalContacto": codigoPostalC.value,
            "d_api": apiCodigo ? 1 : 0,
            "d_entidadContacto": entidadFederativaC.value,
            "telefonos": telefonoC.value,

            /**
             * pagina 03
             */


            "p_id_Estado": dataMenu.infoMenu.estadoSelec.idEstado,
            "p_id_Distrito": dataMenu.infoMenu.distritoFedSelec.idDistrito,
            "p_puesto": cargoDP.value,
            "p_fechaIngreso": fechaIngresoEs.value,
            "p_fechaNombramiento": fechaNombramientoEs.value,
            "p_fechaTitulariad": fechaTitularidadEs.value,
            "p_estatus": 1, // por que esta activo logico
            "p_tipoAlta": 1, // es un nuevo 
            "p_tipoPuesto": puestoEs.value,
            "p_puestoEjerce": puestoDP.value,
            "p_nombramiento": "nombramientoEs.value",
            "p_firma": "firmaEs.value",


            /**
             * pagina 04
             */
            "experiencia": experienciaSend.value
        }
        console.log("el json ", data);

        if (
            cargoDP.estatus&&
            nombreDP.estatus &&
            fotografiaDP.estatus &&
            firmaEs.estatus &&
            nombramientoEs.estatus &&
            claveElectorDP.estatus &&
            curpDP.estatus &&
            generoDP.estatus &&
            fechaNacimientoDP.estatus &&
            ciudadanaDP.estatus &&
            entidadDP.estatus &&
            municipioDP.estatus &&
            calleC.estatus &&
            codigoPostalC.estatus &&
            entidadFederativaC.estatus &&
            municipioC.estatus &&
            coloniaC.estatus &&
            correoC.estatus &&
            fechaIngresoEs.estatus &&
            puestoEs.estatus &&
            profesionDP.estatus &&
            tratamientoDP.estatus &&
            (telefonoC.estatus && telefonoC.value.length > 0) &&
            (experienciaSend.estatus && experienciaSend.value.length > 0) &&
            (apellidoMaternoDP.estatus || apellidoPaternoDP.estatus)

        ) {

            console.log("paso!")
            console.log("el json ", data);

            axios.get(window.location.origin + "/JsonHelpers/jsonUrl.json")
                .then((response) => {
                    // console.log("urls::::::::::");
                    console.log(response);

                    let token = localStorage.getItem('accessToken');
                    if (response.status === 200) {
                        //setUrls(response.data);
                        axios.post(`${response.data.centralSesiones}/vocales/insertar`, data, {
                            headers: {
                                Authorization: token,
                            },
                        }).then((respuesta) => {
                            console.log(respuesta.data)
                            if (respuesta.data.code == 200) {
                                ///alertINE("success", "Se agrego correctamente.");
                                //const idVocalNew = respuesta.data.causa;
                                generarFormImagenes(parseInt(respuesta.data.causa));
                            } else {
                                alertINE("error", "Ocurrio un error al insertar.");
                            }

                        }).catch((erorr) => {
                            console.log("eroror ", erorr)
                        });
                    }
                });


        } else {
            alertINE("error", "Debes llenar todos los campos.");
        }

    }


    const generarFormImagenes = (idNuevo) => {

        const formData = new FormData();

        const listaNombres = ["0.png", "1.pdf", "2.png"];
        var contentType = 'image/png';
        //console.log("ekl origin ", fotografiaDP.value.originFileObj)
        formData.append('files', fotografiaDP.value.originFileObj);
        formData.append('files', nombramientoEs.value.originFileObj);
        formData.append('files', firmaEs.value.originFileObj);

        formData.append('param', new Blob([JSON.stringify({
            idEstado: dataMenu.infoMenu.estadoSelec.idEstado,
            idDistrito: dataMenu.infoMenu.distritoFedSelec.idDistrito,
            modulo: "Vocales",
            idSubModulo: idNuevo,
            nombreSistema: "junta",
            modificar: true,
            nombreArchivo: listaNombres,
        })], { type: "application/json" }));

        console.log("el formData ", formData);
        guardaArchivo(formData).then((data) => {
            alertINE("success", "Seguardo con exito")

            setGuardar(true);
        }).catch((error) => {
            console.log("error guardar !", error)
        });
        return formData;
    }

    const guardaArchivo = (multipart) => {
        return new Promise((resolve, reject) => {
            axios.get(window.location.origin + "/JsonHelpers/jsonUrl.json").then((response) => {
                // console.log(response);
                if (response.status === 200) {
                    const url = response.data.serverGeneral + "/guardarArchivo";
                    let token = localStorage.getItem("accessToken");
                    console.log("el token para insertar DomiciliosDeJunta: ", token);

                    if (token !== undefined && token != null) {
                        console.log("paso a el token ", multipart);
                        axios
                            .post(url, multipart, {
                                headers: {
                                    Authorization: token,
                                },
                            })
                            .then((response) => {
                                // console.log("Exito sevice ", response);
                                localStorage.setItem("accessToken", response.data.token);
                                const temp = response.data;
                                resolve({ err: false, data: temp });
                            })
                            .catch((error) => {
                                console.log("Errror ! service  guardarARchivo", error)

                                resolve({
                                    err: true,
                                    mensaje: error,
                                });
                            });
                    }
                }
            });
        });
    }

    /**
     * valida si lo que ingresa es valido 
     * @param {*} content 
     * @param {*} id 
     * @param {*} name 
     * @param {*} regex 
     * @param {*} warningMessage 
     * @param {*} disabled 
     */
    const handleInput = (content, id, name, regex, warningMessage, disabled) => {
        //console.log("disabled pppp " + disabled)
        //setCheckBoxNum(disabled)
        //console.log(regex)
        var regexResult = regex.test(content);
        switch (name) {
            case "apellidoPaterno":
                //console.log("entro apellido",regexResult, "contenid ",content)
                setApellidoPaternoDP({ estatus: regexResult, value: content });
                break;
            case "apellidoMaterno":
                setApellidoMaternoDP({ estatus: regexResult, value: content });
                break;
            case "nombre":
                setNombreDP({ estatus: regexResult, value: content });
                break;
            case "claveElector":
                if (regexResult) {
                    //const fetchRespuesta =;

                    fetchClaveElector(content).then((data) => {
                        console.log("----------", data);
                        if (data.tipo == 1) { // se puede usar la clave 
                            setClaveElectorDP({ estatus: true, value: content });

                        }
                        if (data.tipo == 2) { // no se pudee usar esa clavae
                            //setIdVocalCurp(data.data.v_id);
                            setClaveElectorDP({ estatus: true, value: ' ' })
                            setAlertOpen(true);
                            setOpenEditar(false);
                            setMensjeOpenAler("La clave de elector ya cuenta con un vocal activo")
                        }
                        if (data.tipo == 3) { //no se pude usar esa clave pero se direcciona 
                            setIdVocalCurp(data.data.v_id);
                            setClaveElectorDP({ estatus: false, value: '  ' })
                            setOpenEditar(true)
                            setAlertOpen(true);
                            setMensjeOpenAler("La clave de elector ya tiene datos quieres modificar")
                        }
                    });
                }
                break;
            case "curp":
                setCurpDP({ estatus: regexResult, value: content })
                break;
            case "puestoEjerce":
                setPuestoDP({ estatus: regexResult, value: content })
                break;
            case "calle":
                setCalleC({ estatus: regexResult, value: content })
                break;
            case "numeroExt":
                setNumeroExteriorC({ estatus: regexResult, value: content })
                break;
            case "numeroInt":
                setNumeroInteriorC({ estatus: regexResult, value: content })
                break;
            case "codigoPostal":

                if (regexResult) {
                    //console.log('**********************' + content + ' exito'
                    setCodigoPostalC({ estatus: true, value: content })


                    getCodigoPolstal(content).then((data) => {
                        if (!data.error) {
                            console.log("se encontro ene el api");
                            setEntidadFederativaC({ estatus: true, value: data.entidad });

                            setMunicipioC({ estatus: true, value: data.municipio });

                            setColonias(data.colonias);
                            setApiCodigo(true);
                        } else {
                            console.log("no se encontro en el api ");
                            setEntidadFederativaC({ estatus: false, value: null });
                            setMunicipioC({ estatus: false, value: null });
                            setColoniaC({ estatus: false, value: null });
                            setApiCodigo(false);
                        }
                    });
                }
                break;
            case "entidad":
                setEntidadFederativaC({ estatus: regexResult, value: content })
                break;
            case "municipio":
                setMunicipioC({ estatus: regexResult, value: content })

                break;
            case "colonia":
                setColoniaC({ estatus: regexResult, value: content })

                break;

            case "correo":
                setCorreoC({ estatus: regexResult, value: content })

                break;


            default:
                break;
        }

    }

    const handleClickOpen = () => {
        setAlertOpen(true);
    };

    const handleClose = () => {
        setClaveElectorDP({ estatus: true, value: '  ' })
        setAlertOpen(false);

    };
    const editarVocalClave = () => {

    }
    const fetchClaveElector = async (claveElector) => {
        console.log("entro ")

        console.log("ago el fetch  ")
        return new Promise((resolve, reject) => {

            axios.get(window.location.origin + '/JsonHelpers/jsonUrl.json').then((response) => {
                // console.log("urls::::::::::");
                //console.log(response);
                //localStorage.setItem('aplicaciones', response.data.urlLogin);
                let token = localStorage.getItem('accessToken');
                //console.log('el token para insertar DomiciliosDeJunta: ', token);
                if (response.status === 200) {
                    //setUrls(response.data);

                    axios
                        .post(`${response.data.centralSesiones}/vocales/claveElector`, {
                            "claveElector": claveElector
                        }, {
                            headers: {
                                Authorization: token,
                            },
                        })
                        .then((respuesta) => {
                            //localStorage.setItem("accessToken", response.data.token);

                            console.log(respuesta);
                            if (respuesta.data.code == 200) {
                                let dataresponse = {
                                    tipo: respuesta.data.causa,
                                    data: respuesta.data.vocal
                                }
                                //caso                                
                                resolve(dataresponse);
                            } else {
                                alertINE("Ocurrio un erro con la clave elector")
                                resolve({ err: false, data: "ocurrio errro " });
                            }
                        })
                        .catch((error) => {
                            console.log('eroror ', error);
                            alertINE("Ocurrio un error en el servicio  Clave electro")
                            reject({ err: true, data: "ocurrio errro " + error });
                        });
                }
            });

        });
    }


    /**
     * 
     * eventos que cambian 
     *  
     */



    const changeCargo = e => {
        console.log("cargo ", e)
        setCargoDP({ estatus: true, value: e })
    }

    const changeGenero = e => {
        //console.log(e)
        console.log("genero ", e.target.value)
        setGeneroDP({ estatus: true, value: e.target.value })
    }
    const changeCiudanania = e => {
        console.log("ciudano ", e.target.value)
        setCiudadanaDP({ estatus: true, value: e.target.value })
    }

    const changeFechadeNacimiento = (data, dateString) => {
        console.log("fecha Nacimeinto ", dateString)
        setFechaNacimientoDP({ estatus: true, value: dateString });
    }
    const changeEntidad = e => {
        console.log("enrtidad ", e)
        setLlenarDistrito(e)
        setMunicipioDP({ estatus: false, value: null })
        setEntidadDP({ estatus: true, value: e })
    }

    const changeMunicipio = e => {
        console.log("Municipio ", e)
        setMunicipioDP({ estatus: true, value: e })
    }
    const changeProfesion = e => {
        console.log("profesion ", e)
        setProfesionDP({ estatus: true, value: e })
    }
    const changeTratamiento = e => {
        console.log("tratamiento ", e)
        setTratamientoDP({ estatus: true, value: e })
    }

    const onChangeCheckBoxNum = e => {
        console.log("checbox ", e)
        setSinNumeroC({ estatus: !e.target.check, value: e.target.check })

    }

    const onChangeCodigoPostal = e => {
        console.log("codigo postal, ", e)

    }
    const selectColonia = e => {
        console.log("selectColonia, ", e)
        setColoniaC({ estatus: true, value: e })
    }
    /**
     * 
     * @param {*} data 
     * @param {*} dateString texto con el formato 
     */


    /**
     * 
     * @param {*} data 
     * @param {*} dateString  el texto con el formato 
     */
    const changeFechaIngreso = (data, dateString) => {
        console.log("fehca Ingreso ", dateString)
        setFechaIngresoEs({ estatus: true, value: dateString });
    }

    /**
     * 
     * @param {*} data 
     * @param {*} dateString el texto con el formato 
     */
    const changeFechaNombramiento = (data, dateString) => {
        console.log("fecha Nombramiento ", dateString)
        setFechaNombramientoEs({ estatus: true, value: dateString });
    }

    /**
     * 
     * @param {*} data 
     * @param {*} dateString 
     */
    const changeFechaTitulariad = (data, dateString) => {
        console.log("fecha de titulariad ", dateString);
        setFechaTitularidadEs({ estatus: true, value: dateString });
    }
    /**
     * 
     * @param {*} e 
     */
    const changePuesto = e => {
        console.log("puesto ", e.target.value)
        setPuestoEs({ estatus: true, value: e.target.value });
    }

    const verifcarVocalSustitucion = () => {
        if (!estatus &&
            //fotografiaDP.estatus &&
            //firmaEs.estatus &&
            //nombramientoEs.estatus &&
            profesionDP.estatus &&
            tratamientoDP.estatus &&
            nombreDP.estatus &&
            claveElectorDP.estatus &&
            curpDP.estatus &&
            generoDP.estatus &&
            fechaNacimientoDP.estatus &&
            ciudadanaDP.estatus &&
            entidadDP.estatus &&
            municipioDP.estatus &&
            calleC.estatus &&
            codigoPostalC.estatus &&
            entidadFederativaC.estatus &&
            municipioC.estatus &&
            coloniaC.estatus &&
            correoC.estatus &&
            fechaIngresoEs.estatus &&
            puestoEs.estatus &&
            (telefonoC.estatus && telefonoC.value.length > 0) &&
            (experienciaSend.estatus && experienciaSend.value.length > 0) &&
            (apellidoMaternoDP.estatus || apellidoPaternoDP.estatus)

        ) {
            alertINE("success", "se puede enviar ")
            let data = {
                /**
                 * pagina 01
                 */
                //puestoDP.value  // falta este dato
                "v_id": null,
                "usuario": null,
                "v_nombre": nombreDP.value,
                "v_apellidoPaterno": apellidoPaternoDP.value,
                "v_apellidoMaterno": apellidoMaternoDP.value,
                "v_claveElector": claveElectorDP.value,
                "v_curp": curpDP.value,
                "v_genero": generoDP.value,
                "v_fechaNacimiento": fechaNacimientoDP.value,
                "v_ciudadania": ciudadanaDP.value,
                "v_entidad": entidadDP.value,
                "v_municipio": municipioDP.value,
                "v_profesion": profesionDP.value,
                "v_tratamiento": tratamientoDP.value,
                "v_img": "fotografiaDP.value",
                "v_correoContacto": correoC.value,
                /**
                 * pagina 02
                 */

                "d_calleContacto": calleC.value,
                "d_numeroExterior": numeroExteriorC.value,
                "d_numeroInterior": numeroInteriorC.value,
                "d_coloniaContacto": coloniaC.value,
                "d_idMunicipioContacto": -1,
                "d_municipioContacto": municipioC.value,
                "d_codigoPostalContacto": codigoPostalC.value,
                "d_api": apiCodigo ? 1 : 0,
                "d_entidadContacto": entidadFederativaC.value,
                "telefonos": telefonoC.value,

                /**
                 * pagina 03
                 */


                "p_id_Estado": dataMenu.infoMenu.estadoSelec.idEstado,
                "p_id_Distrito": dataMenu.infoMenu.distritoFedSelec.idDistrito,
                "p_puesto": cargoDP.value,
                "p_fechaIngreso": fechaIngresoEs.value,
                "p_fechaNombramiento": fechaNombramientoEs.value,
                "p_fechaTitulariad": fechaTitularidadEs.value,
                "p_estatus": 1, // esta activo el vocal 
                "p_tipoAlta": 2, // se pone 2  por que va a sustituir a alguien 
                "p_tipoPuesto": puestoEs.value,
                "p_puestoEjerce": puestoDP.value,
                "p_nombramiento": "nombramientoEs.value",
                "p_firma": firmaEs.value,


                /**
                 * pagina 04
                 */
                "experiencia": experienciaSend.value,
                /**
                 * fotos y archivo 
                 */

                "archivos": [fotografiaDP.value, firmaEs.value, nombramientoEs.value]
            }
            completo({ error: false, data });
        } else {
            alertINE("error", "Debes llenar todos los campos. de el formulario Nuevo Vocal");

        }

    }



    const changeImagen = e => {
        console.log(e)
        setFotografiaDP({ estatus: true, value: e })

    }


    const changeFirma = e => {
        //console.log("valor de la img ", data)
        setFirmaEs({ estatus: true, value: e })
    }

    const changeArchivo = e => {
        console.log("el arvhvio ", e);
        setNombramientoEs({ estatus: true, value: e })
    }






    return (

        <div className="" >

            {
                estatus ? <Link to={{
                    pathname: `/vocales/home`
                }
                } >

                    <u className="letras" ><i className="iconAtras" />Regresar </u>
                </Link> : null
            }
            <h1>Nuevo Registro de Vocal</h1>
            <h6></h6>

            <div>
                <Steps current={numeroPagina.pagina}>

                    <Step title="Datos Personales" />
                    <Step title="Contacto" />
                    <Step title="Estatus" />
                    <Step title="Experiencia" />

                </Steps>
                {/*
                    pagina datos Personales
                  */}
                {numeroPagina.pagina === 0 &&
                    <div className="col-12">
                        <div className="center-block col-md-12 col-sm-12" >
                            <CargarImagen getData={changeImagen} ponerImagen={fotografiaDP.value} texto={"Fotografia"} />
                        </div>
                        <div className="row">
                            <div className="col-md-4 col-sm-12">

                                <font color="#d5007f">*</font>Cargo
                            <br />
                                {estatus ?

                                    <Select onChange={changeCargo}
                                        style={{ width: '100%' }} disabled={editar} value={cargoDP.estatus ? cargoDP.value : "Selecciona una opción"}
                                        placeholder="Selecciona una opción">

                                        {

                                            tempCargo != null ?
                                                tempCargo.map((tem) => (
                                                    <Select.Option value={tem.id}>{tem.descripcion}</Select.Option>
                                                ))
                                                : null
                                        }
                                    </Select>

                                    :
                                    <Select
                                        style={{ width: '100%' }} disabled={editar} value={idVocalBajaPuesto}
                                        placeholder="Selecciona una opción" disabled={true}>
                                        <Select.Option value={-1}>Selecciona una opción</Select.Option>

                                        {cargoSustituir.map((tem) => (
                                            <Select.Option value={tem.id}>{tem.descripcion}</Select.Option>
                                        ))
                                        }

                                    </Select>

                                }
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-12">

                                <Tooltip placement="right" title="Al menos uno de los apellidos es obligatorio">
                                    <i className="tooltip-apellido" />
                                </Tooltip>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-4 col-sm-12">
                                <InputINE className='ant-input-sinIconoIzquierdo' onSelectLanguage={handleInput}
                                    label='Apellido paterno' name='apellidoPaterno'
                                    placeHolder='Apellido paterno'
                                    value={apellidoPaternoDP.estatus ? apellidoPaternoDP.value : ''}
                                    type='text'
                                    regex={/^([\w. áéíóúÁÉÍÓÚñ.,-;#’/])+$/}
                                    // icon={<LockOutlined style={{ color: '#d5007f' }} />}
                                    warningMessage='Apellido paterno con caracteres inválidos'
                                    tooltipMessage='Apellido paterno'
                                    paddingLeft='3px' />
                            </div>
                            <div className="col-md-4 col-sm-12">
                                <InputINE className='ant-input-sinIconoIzquierdo' onSelectLanguage={handleInput}
                                    label='Apellido materno' name='apellidoMaterno'
                                    placeHolder='Apellido materno'
                                    value={apellidoMaternoDP.estatus ? apellidoMaternoDP.value : ''}
                                    type='text'
                                    regex={/^([\w. áéíóúÁÉÍÓÚñ.,-;#’/])+$/}
                                    // icon={<LockOutlined style={{ color: '#d5007f' }} />}
                                    warningMessage='Apellido materno con caracteres inválidos'
                                    tooltipMessage='Apellido materno'
                                    paddingLeft='3px' />
                            </div>
                            <div className="col-md-4 col-sm-12">
                                <font color="#d5007f">*</font>Nombre(s)
                                <InputINE className='ant-input-sinIconoIzquierdo' onSelectLanguage={handleInput}
                                    label=''
                                    name='nombre'
                                    placeHolder='Nombre(s)'
                                    value={nombreDP.estatus ? nombreDP.value : ''}
                                    type='text'
                                    regex={/^([\w. áéíóúÁÉÍÓÚñ.,-;#’/])+$/}
                                    // icon={<LockOutlined style={{ color: '#d5007f' }} />}
                                    warningMessage=' Nombre(s) con caracteres inválidos'
                                    tooltipMessage=' Nombre(s)'
                                    paddingLeft='3px' />
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-4 col-sm-12">
                                <font color="#d5007f">*</font>Clave de elector
                                <InputINE className='ant-input-sinIconoIzquierdo' onSelectLanguage={handleInput}
                                    label='' name='claveElector'
                                    placeHolder='AAAAAA12345678A123'
                                    value={claveElectorDP.estatus ? claveElectorDP.value : ''}
                                    type='text'
                                    // regex={/^([\w. áéíóúÁÉÍÓÚñ.,-;#’/])+$/}
                                    regex={/^([a-zA-Z]{5,6}\d{8}[H|M|m|h]\d{3})$/}
                                    // icon={<LockOutlined style={{ color: '#d5007f' }} />}
                                    warningMessage='Clave de elector con caracteres inválidos'
                                    tooltipMessage='Clave de elector'
                                    paddingLeft='3px' />
                            </div>
                            <div className="col-md-4 col-sm-12">
                                <font color="#d5007f">*</font>CURP
                                <InputINE className='ant-input-sinIconoIzquierdo' onSelectLanguage={handleInput}
                                    label='' name='curp'
                                    placeHolder='AAAA123456AAABBB01'
                                    value={curpDP.estatus ? curpDP.value : ''}
                                    type='text'
                                    regex={/^([\w. áéíóúÁÉÍÓÚñ.,-;#’/])+$/}
                                    // icon={<LockOutlined style={{ color: '#d5007f' }} />}
                                    warningMessage='CURP con caracteres inválidos'
                                    tooltipMessage='CURP'
                                    paddingLeft='3px' />
                            </div>
                            <div className="col-md-4 col-sm-12">
                                <div>
                                    <font color="#d5007f">*</font>Genero
                                <br />
                                    <Radio.Group onChange={changeGenero} value={generoDP.estatus ? generoDP.value : ""}
                                    >
                                        <Radio value={"M"}>Femenino</Radio>
                                        <Radio value={"H"}>Masculino</Radio>

                                    </Radio.Group>
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-5 col-sm-12">
                                <div>
                                    <font color="#d5007f">*</font>Fecha de nacimiento
                                <br />
                                    <DatePicker placeHolder={"DD/MM/AAAA"} value={fechaNacimientoDP.estatus ? moment(fechaNacimientoDP.value, "DD-MM-YYYY") : ""} format={'DD/MM/YYYY'} onChange={(evento, fecha) => changeFechadeNacimiento(evento, fecha)} />
                                </div>
                            </div>
                            <div className="col-md-7 col-sm-12">
                                <div>
                                    <font color="#d5007f">*</font>Ciudadana/o mexicana/o
                                <br />
                                    <Radio.Group onChange={changeCiudanania} value={ciudadanaDP.estatus ? ciudadanaDP.value : ""} >
                                        <Radio value={1}>Nacida/o en territorio mexicano</Radio>
                                        <Radio value={2}>Nacida/o en el extranjero</Radio>
                                        <Radio value={3}>Nacionalizada/o</Radio>
                                    </Radio.Group>
                                </div>
                            </div>

                        </div>


                        <div className="row">
                            <div className="col-md-4 col-sm-12">
                                <font color="#d5007f">*</font>Entidad Federativa de nacimiento
                                <br />
                                <Select style={{ width: '100%' }} onChange={changeEntidad} value={entidadDP.estatus ? entidadDP.value : "Selecciona una opción"}
                                    placeholder="Selecciona una opción">
                                    {
                                        Estados != null ?
                                            Estados.estados.map((tem) => (
                                                <Select.Option value={tem.idEstado}>{tem.nombreEstado}</Select.Option>
                                            ))
                                            : null

                                    }
                                </Select>
                            </div>
                            <div className="col-md-4 col-sm-12">
                                <font color="#d5007f">*</font>Municipio de nacimiento
                                <br />
                                <Select onChange={changeMunicipio} style={{ width: '100%' }} value={municipioDP.estatus ? municipioDP.value : "Selecciona una opción"}
                                    placeholder="Selecciona una opción">
                                    {
                                        llenarDistrito != null ?
                                            Estados.estados[(llenarDistrito - 1)].distritos.map((tem) => (
                                                <Select.Option value={tem.idDistrito}>{tem.nombreDistrito}</Select.Option>
                                            ))
                                            : null
                                    }
                                </Select>
                            </div>
                            <div className="col-md-4 col-sm-12">

                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-4 col-sm-12">
                                <font color="#d5007f">*</font>Profesión
                                <br />
                                <Select onChange={changeProfesion}
                                    style={{ width: '100%' }} value={profesionDP.estatus ? profesionDP.value : "Selecciona una opción"}
                                    placeholder="Selecciona una opción">
                                    {

                                        tempProfesiones != null ?
                                            tempProfesiones.map((tem) => (
                                                <Select.Option value={tem.idProfesion}>{tem.descripcion}</Select.Option>
                                            ))
                                            : null
                                    }
                                </Select>
                            </div>
                            <div className="col-md-4 col-sm-12">
                                <font color="#d5007f">*</font>Tratamiento
                                <br />
                                <Select onChange={changeTratamiento}
                                    style={{ width: '100%' }} value={tratamientoDP.estatus ? tratamientoDP.value : "Selecciona una opción"}
                                    placeholder="Selecciona una opción">
                                    {
                                        tempTratamiento != null ?
                                            tempTratamiento.map((tem) => (
                                                <Select.Option value={tem.idTratamiento}>{tem.descripcion}</Select.Option>
                                            ))
                                            : null

                                    }
                                </Select>
                            </div>
                            <div className="col-md-4 col-sm-12">
                                <InputINE className='ant-input-sinIconoIzquierdo' onSelectLanguage={handleInput}
                                    label='Puesto que ejerce' name='puestoEjerce'
                                    placeHolder='Ingresa información'
                                    value={puestoDP.estatus ? puestoDP.value : ''}
                                    type='text'
                                    regex={/^([\w. áéíóúÁÉÍÓÚñ.,-;#’/])+$/}
                                    // icon={<LockOutlined style={{ color: '#d5007f' }} />}
                                    warningMessage='Puesto que ejerce con caracteres inválidos'
                                    tooltipMessage='Puesto que ejerce'
                                    paddingLeft='3px' />
                            </div>
                        </div>

                    </div>
                }

                {/*
                    Contacto 
                  */}
                {numeroPagina.pagina === 1 &&
                    <div className="steps-content">
                        <br />
                        <div class="form-row mt-4">
                            <div class="col-sm-4 pb-3">
                                <font color="#d5007f">*</font>Calle
                                <InputINE className='ant-input-sinIconoIzquierdo' onSelectLanguage={handleInput}
                                    label='' name='calle'
                                    placeHolder='Ingresa calle'
                                    value={calleC.estatus ? calleC.value : ''}
                                    type='text'
                                    regex={/^([\w. áéíóúÁÉÍÓÚñ.,-;#’/])+$/}
                                    // icon={<LockOutlined style={{ color: '#d5007f' }} />}
                                    warningMessage='Calle con caracteres inválidos'
                                    tooltipMessage='Calle'
                                    paddingLeft='3px' />
                            </div>
                            <div class="col-sm-2 pb-3">
                                {/* {console.log("quitar ", checkBoxNum)} */}
                                {sinNumeroC.estatus ?
                                    <div>
                                        No. exterior
                                <p className='h45'>S/N</p>
                                    </div>
                                    :
                                    <>
                                        <font color="#d5007f">*</font>No. exterior
                                    <InputINE onSelectLanguage={handleInput}
                                            label='' name='numeroExt'
                                            placeHolder='Número ext.'
                                            value={numeroExteriorC.estatus ? numeroExteriorC.value : ''}
                                            type='text'
                                            regex={/^\d+$/}
                                            // icon={<LockOutlined style={{ color: '#d5007f' }} />}
                                            warningMessage='Número exterior sólo debe contener números'
                                            tooltipMessage='Número exterior'
                                            paddingLeft='3px'
                                        />
                                    </>
                                }
                            </div>
                            <div class="col-sm-2 pb-3">
                                {sinNumeroC.estatus ?
                                    <div>
                                        No. interior
                                <p className='h45'>S/N</p>
                                    </div>
                                    :
                                    <InputINE onSelectLanguage={handleInput}
                                        label='No. interior' name='numeroInt'
                                        placeHolder='Número int.'
                                        value={numeroInteriorC.estatus ? numeroInteriorC.value : ''}
                                        type='text'
                                        regex={/^\d+$/}
                                        // icon={<LockOutlined style={{ color: '#d5007f' }} />}
                                        warningMessage='Número interior sólo debe contener números'
                                        tooltipMessage='Número interior'
                                        paddingLeft='3px'
                                    />
                                }
                            </div>
                            {/* checked={numeroExterior==-1&&numeroInterior==-1?true:false} */}
                            <div class="col-sm-2 pb-3">
                                <Checkbox style={{ paddingTop: '36px' }} onChange={onChangeCheckBoxNum}>Sin número</Checkbox>

                            </div>
                            <div class="col-sm-2 pb-3">
                                <font color="#d5007f">*</font>C.P.
                                <InputINE onSelectLanguage={handleInput}
                                    onChange={onChangeCodigoPostal}
                                    label='' name='codigoPostal'
                                    placeHolder='12345'
                                    value={codigoPostalC.estatus ? codigoPostalC.value : ''}
                                    type='text'
                                    regex={/^\d{5}$/}
                                    // icon={<LockOutlined style={{ color: '#d5007f' }} />}
                                    warningMessage='Código Postal sólo debe contener números'
                                    tooltipMessage='Código Postal'
                                    paddingLeft='3px'
                                    style={{ disabled: true }}
                                />
                            </div>
                        </div>

                        <div class="form-row mt-4">
                            <div class="col-sm-4 pb-3">
                                {apiCodigo ?
                                    <div>
                                        <font color="#d5007f">*</font>Entidad
                            <p className='h45'>{entidadFederativaC.value}</p>
                                    </div>
                                    :
                                    <>
                                        <font color="#d5007f">*</font>Entidad
                                    <InputINE onSelectLanguage={handleInput}
                                            label='' name='entidad'
                                            placeHolder='Ingresa Entidad'
                                            type='text'
                                            regex={/^([\w. áéíóúÁÉÍÓÚñ.,-;#’/])+$/}
                                            value={entidadFederativaC.estatus ? entidadFederativaC.value : ''}
                                            // icon={<LockOutlined style={{ color: '#d5007f' }} />}
                                            warningMessage='Entidad tiene caracteres inválidos'
                                            tooltipMessage='Entidad '
                                            paddingLeft='3px' />
                                    </>
                                }
                            </div>

                            <div class="col-sm-4 pb-2">
                                {apiCodigo ?
                                    <div>
                                        <font color="#d5007f">*</font>Municipio
                                        <p className='h45'>{municipioC.value}</p>
                                    </div>
                                    :
                                    <>
                                        <font color="#d5007f">*</font>Municipio
                                    <InputINE onSelectLanguage={handleInput}
                                            label='' name='municipio'
                                            placeHolder='Ingresa Municipio'
                                            type='text'
                                            regex={/^([\w. áéíóúÁÉÍÓÚñ.,-;#’/])+$/}
                                            // icon={<LockOutlined style={{ color: '#d5007f' }} />}
                                            value={municipioC.estatus ? municipioC.value : ''}
                                            warningMessage='Municipio tiene caracteres inválidos'
                                            tooltipMessage='Municipio'
                                            paddingLeft='3px' >
                                        </InputINE>
                                    </>
                                }
                            </div>
                            <div class="col-sm-4 pb-2">
                                <font color="#d5007f">*</font>Colonia
                            {
                                    apiCodigo ?

                                        <Select style={{ width: '100%' }} onChange={selectColonia} value={coloniaC.estatus ? coloniaC.value : ''} >
                                            {colonias.map(tem =>

                                                <Select.Option value={tem}>{tem}</Select.Option>

                                            )}
                                        </Select>
                                        :
                                        <InputINE onSelectLanguage={handleInput}
                                            label='' name='colonia'
                                            placeHolder='Ingresa tu colonia'
                                            type='text'
                                            value={coloniaC.estatus ? coloniaC.value : ''}
                                            regex={/^([\w. áéíóúÁÉÍÓÚñ.,-;#’/])+$/}
                                            warningMessage='Colonia tiene caracteres inválidos'
                                            tooltipMessage='Colonia'
                                            paddingLeft='3px' />
                                }

                            </div>
                        </div>

                        <h3>Contacto</h3>
                        <div class="form-row mt-4">
                            <div class="col-sm-4 pb-3">
                                <font color="#d5007f">*</font>Correo
                                <InputINE onSelectLanguage={handleInput}
                                    label='' name='correo'
                                    placeHolder='nombre@dominio.com'
                                    value={correoC.estatus ? correoC.value : ''}
                                    type='text'
                                    regex={/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/}
                                    // icon={<LockOutlined style={{ color: '#d5007f' }} />}
                                    warningMessage='Correo tiene caracteres inválidos'
                                    tooltipMessage='Correo'
                                    paddingLeft='3px' />
                            </div>
                            <div class="col-sm-4 pb-3">
                                <font color="#d5007f">*</font>Teléfono
                                <Telefonos getData={changeTelefono} lista={telefonoC.value} />
                            </div>
                        </div>
                        <div><p /><p /><p /></div>
                    </div>
                }

                {/*
                    Estatus
                  */}
                {numeroPagina.pagina === 2 &&
                    <div className="steps-content">
                        <div className="row">
                            <div className="col-md-4 col-sm-12">
                                <font color="#d5007f">*</font>Fecha de ingreso
                                <br />
                                <DatePicker placeHolder={"DD/MM/AAAA"} format={'DD/MM/YYYY'} value={fechaIngresoEs.estatus ? moment(fechaIngresoEs.value, "DD-MM-YYYY") : ""} onChange={(evento, fecha) => changeFechaIngreso(evento, fecha)} />
                            </div>
                            <div className="col-md-4 col-sm-12">
                                Fecha de nombramiento
                                <br />
                                <DatePicker placeHolder={"DD/MM/AAAA"} format={'DD/MM/YYYY'} value={fechaNombramientoEs.estatus ? moment(fechaNombramientoEs.value, "DD-MM-YYYY") : ""} onChange={(evento, fecha) => changeFechaNombramiento(evento, fecha)} />
                            </div>
                            <div>
                                Fecha de titularidad
                                <br />
                                <DatePicker placeHolder={"DD/MM/AAAA"} format={'DD/MM/YYYY'} value={fechaTitularidadEs.estatus ? moment(fechaTitularidadEs.value, "DD-MM-YYYY") : ""} onChange={(evento, fecha) => changeFechaTitulariad(evento, fecha)} />
                            </div>

                        </div>
                        <div className="row">
                            <div className="col-md-4 col-sm-12">
                                <font color="#d5007f">*</font>Nombramiento
                            <br />
                                <CargarArchivo getData={changeArchivo} ponerArchivo={nombramientoEs.value} />
                            </div>
                            <div className="col-md-8 col-sm-12">
                                <font color="#d5007f">*</font>Puesto
                                <br />
                                <Radio.Group onChange={changePuesto} value={puestoEs.estatus ? puestoEs.value : ""}>
                                    <Radio value={1}>Propietario</Radio>
                                    <Radio value={2}>Encargado del Despacho</Radio>
                                </Radio.Group>
                            </div>
                            <div className="col-md-12 col-sm-12">
                                Captura firma
                                <br />
                                <CargarImagen getData={changeFirma} texto={"Firma"} ponerImagen={firmaEs.value} />
                            </div>
                        </div>
                    </div>
                }

                {/*
                    experiencia 
                  */}
                {numeroPagina.pagina === 3 &&
                    <div className="steps-content">
                        <div className="">
                            <Experiencia puestos={listaPuestos} getDataSend={changeExperienciaSend} getData={changeExperiencia} listExperienciaSend={experienciaSend.value} listExperiencia={experiencia.value} />
                        </div>

                    </div>
                }


                <div className="steps-action row espacios">

                    {numeroPagina.pagina == 0 &&
                        <div className="col-md-12 col-sm-6">

                            <u className="letras float-right" onClick={next}>Siguiente <i className="iconSigueinte" /></u>
                        </div>
                    }
                    {numeroPagina.pagina < 3 && numeroPagina.pagina > 0 &&
                        <>
                            <div className="col-md-6 col-sm-6">
                                <u className="letras" onClick={prev}><i className="iconAtras" />Atras </u>
                            </div>
                            <div className="col-md-6 col-sm-6">

                                <u className="letras float-right" onClick={next}>Siguiente <i className="iconSigueinte" /></u>
                            </div>
                        </>
                    }

                    {
                        numeroPagina.pagina === 3 &&
                        <>
                            <div className="col-md-6 col-sm-6">
                                <u className="letras" onClick={prev}><i className="iconAtras" />Atras </u>
                            </div>
                            {
                                estatus ?
                                    <div className="col-md-6 col-sm-12 ">
                                        <Button type="primary" onClick={enviar}>
                                            Enviar
                                </Button>
                                    </div>
                                    : //verifcarVocalSustitucion
                                    <div className="col-md-6 col-sm-12 ">
                                        <Button type="primary" onClick={verifcarVocalSustitucion}>
                                            Verficar Formulario
                                </Button>
                                    </div>
                            }
                        </>
                    }

                </div>

                <Dialog open={guardar} onClose={handleClose}>
                    <DialogContent>Se agrego con exito   </DialogContent>
                    <DialogActions>
                        <Link to={`/vocales/home`}>
                            <Button onClick={editarVocalClave}>Aceptar</Button>
                        </Link>
                    </DialogActions>
                </Dialog>

                <Dialog open={alertOpen} onClose={handleClose}>
                    <DialogContent>{mensajeOpenAlert} </DialogContent>
                    <DialogActions>
                        {
                            editarOpen ?
                                <Link to={`/vocales/${idVocalCurp}/editar`}>
                                    <Button onClick={editarVocalClave}>Aceptar</Button>
                                </Link>
                                :
                                <Button onClick={handleClose}>Aceptar</Button>
                        }
                        {
                            editarOpen ?
                                <Button onClick={handleClose}>Cancelar</Button>
                                : null
                        }
                    </DialogActions>
                </Dialog>
            </div>

        </div>
    )
}




