import * as ActionTypes from './Actions';

// ACTIONS PARA EL INICIO Y CIERRE DE SESION DEL SISTEMA

export const loginSistema = (data, accessToken, menu) => ({
    type: ActionTypes.LOGIN_SISTEMA,
    isLogged: true,
    data: data,
    accessToken: accessToken,
    menu: menu,
    messageError: null
});

export const logoutSistema = () => ({
    type: ActionTypes.LOGOUT_SISTEMA,
    isLogged: false,
    data: [],
    token: [],
    menu: [],
    messageError: null
});

export const logoutSistemaError = (error) => ({
    type: ActionTypes.LOGOUT_SISTEMA_ERROR,
    isLogged: false,
    data: [],
    token: [],
    menu: [],
    messageError: error
});