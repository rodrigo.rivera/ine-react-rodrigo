import axios from "axios";
/**
 * Consume un servicio para obtener las asocicones
 * @param {*} proceso
 * @param {*} estado
 * @param {*} distrito
 */

export function getDomicilios(estado, distrito) {
  return new Promise((resolve, reject) => {
    //console.log("proceso ", proceso, "estado: ", estado, "distrito:", distrito)

    const data = {
      idEstado: estado,
      idDistrito: distrito,
      modulo: "Domicilios",
      modificar: true,

    };
    axios
      .get(window.location.origin + "/JsonHelpers/jsonUrl.json")
      .then((response) => {
        console.log(response);
        if (response.status === 200) {
          const url = response.data.centralSesiones + "/domicilios/consulta";
          // console.log("[", url, " ]")
          // console.log(data)
          // console.log("_______________________LLAMADA")
          let token = localStorage.getItem("accessToken");
          //console.log("el token serice Acreditacion", token);

          if (token !== undefined && token !== null) {
            // console.log("paso a el topklen en service Domicilios consulta");
            axios
              .post(url, data, {
                headers: {
                  Authorization: token,
                },
              })
              .then((response) => {
                console.log("Exito sevice Domicilios de Junta ", response);
                localStorage.setItem("accessToken", response.data.newToken);
                const temp = response.data;
                resolve({ error: false, data: temp });
              })
              .catch((err) => {
                console.log("Errror ! service getDomicilios de Junta  ", err)

                resolve({
                  erorr: true,
                  mensaje: err,
                });

              });
          }
        }
      });
  });
}

export function guardaArchivo(multipart) {
  return new Promise((resolve, reject) => {
    axios
      .get(window.location.origin + "/JsonHelpers/jsonUrl.json")
      .then((response) => {
        // console.log(response);
        if (response.status === 200) {
          const url = response.data.serverGeneral + "/guardarArchivo";
          let token = localStorage.getItem("accessToken");
          // console.log("el token para insertar DomiciliosDeJunta: ", token);

          if (token !== undefined && token != null) {
            // console.log("paso a el token en InsertarDomiciliosDeJunta");
            axios
              .post(url, multipart, {
                headers: {
                  Authorization: token,
                },
              })
              .then((response) => {
                // console.log("Exito sevice ", response);
                localStorage.setItem("accessToken", response.data.token);
                const temp = response.data;
                resolve({ err: false, data: temp });
              })
              .catch((error) => {
                console.log("Errror ! service  guardarARchivo", error)

                resolve({
                  err: true,
                  mensaje: error,
                });
              });
          }
        }
      });
  });
}


export function insertarDomicilio(data) {
  // console.log(typeof data);
  return new Promise((resolve, reject) => {
    // console.log("se va insertar ", data);

    axios.get(window.location.origin + "/JsonHelpers/jsonUrl.json")
      .then((response) => {
        // console.log("respueta ", response);
        if (response.status === 200) {
          const url = response.data.centralSesiones + "/domicilios/inserta";
          // console.log("direccion add :", url)
          axios.post(url, data, {
            headers: {
              Authorization: localStorage.getItem("accessToken"),
            },
          })
            .then((response) => {
              // console.log("exito add !!", response);
              //  localStorage.setItem("accessToken", response.data.newToken);
              resolve({
                error: false,
                mensaje: response,
                data: response.data,
                id: response.data.idDireccionJunta
              });
            })
            .catch((err) => {
              //console.log("ocurrio un error", err);
              resolve({
                errr: true,
                mensaje: err,
              });
            });
        }
      });
  });
}

/**
 *
 * @param {*} id
 */
export function deleteDomicilio(idDirJuntaEjecutiva) {
  return new Promise((resolve, reject) => {
    console.log("id que se va eliminar ", idDirJuntaEjecutiva);

    axios
      .get(window.location.origin + "/JsonHelpers/jsonUrl.json")
      .then((response) => {
        console.log(response);
        if (response.status === 200) {
          const url = response.data.centralSesiones + "/domicilios/elimina";
          console.log("direccion a donde se va enviar ", url);
          console.log("ELIMINAR ID DIR JUNTA " + idDirJuntaEjecutiva)
          const data = {
            idDirJuntaEjecutiva: idDirJuntaEjecutiva,
          };
          axios
            .post(url, data, {
              headers: {
                Authorization: localStorage.getItem("accessToken"),
              },
            })
            .then((response) => {
              console.log("log de eliminar : ", response);
              localStorage.setItem("accessToken", response.data.newToken);
              resolve({
                error: false,
                data: response,
              });
            })
            .catch((erro) => {
              resolve({
                error: true,
                data: erro,
              });
            });
        }
      });
  });
}
