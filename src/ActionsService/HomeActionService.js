import {
  loginSistema,
  logoutSistema,
  logoutSistemaError,
} from "../Actions/HomeActionCreator";
import Cookies from "js-cookie";
import axios from "axios";

export const fetchLogin = (data, accessToken, menu) => (dispatch) => {
  return dispatch(loginSistema(data, accessToken, menu));
};

export const fetchLogout = (accessToken) => (dispatch) => {
  axios
    .get(window.location.origin + "/JsonHelpers/jsonUrl.json")
    .then((response) => {
      console.log(response);
      if (response.status === 200) {
        return axios
          .post(
            response.data.wsUrlPath + "/cierraSesion",
            {},
            {
              headers: {
                Authorization: accessToken,
              },
            }
          )
          .then((resp) => {
            console.log("Exito !!  ", resp);
            logoutSistema();

            Cookies.remove("us-ses", { path: "/", domain: response.data.cookieDomain });
            Cookies.remove("cn-ses", { path: "/", domain: response.data.cookieDomain });
            Cookies.remove("vr-ses", { path: "/", domain: response.data.cookieDomain });
            Cookies.remove("sy-ses", { path: "/", domain: response.data.cookieDomain });
            Cookies.remove("mg-ses", { path: "/", domain: response.data.cookieDomain });

            window.location.href = response.data.urlLogin + "/sesiones/login";
          })
          .catch((err) => {
            console.log("Error!  ", err);
            return logoutSistemaError(err);
          });
      }
    });
};

/**
 * obtiene lista de entidades
 * @param {*} data
 */
export const getMenu = (data) => {
  return new Promise((resolve) => {
    const url = localStorage.getItem('junta-wsUrlPath') + "/cambiaInfoMenu";

    axios
      .post(url, data, {
        headers: {
          Authorization: localStorage.getItem("accessToken"),
        },
      })
      .then((response) => {
        localStorage.setItem("accessToken", response.data.newToken);
        resolve({
          err: false,
          data: response.data,
        });
      })
      .catch((err) => {
        console.log("Error proceso ! " + err);
        resolve({
          err: true,
          data: err
        });
      });
  });
};


export const llenaData = (data, repuesta) => {


}