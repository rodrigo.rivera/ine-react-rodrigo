import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Steps, Button, Select, Radio, DatePicker, Checkbox, message } from 'antd';
import InputINE from './Input/InputINE';
import './Vocales.css';

const { Step } = Steps;
const { Option } = Select;

const mapStateToProps = (state) => {
    return {
        inicio: state.inicio
    }
  }
  
  const mapDispatchToProps = (dispatch) => ({
    // fetchLogin: (data, accessToken, menu, urls) => dispatch(fetchLogin(data, accessToken, menu, urls))
  });

class VocalesFormulario extends Component {
    state = {
        pagina: 0,
        checkBoxNum : false,
        //pagina 01
        fotografiaDP: '',
        apellidoMaternoDP: '',
        apellidoPaternoDP: '',
        nombreDP: '',
        claveElectorDP: '',
        curpDP: '',
        generoDP: '',
        fechaNacimientoDP: '',
        ciudadanaDP: '',
        entidadDP: '',
        municipioDP: '',
        profesionDP: '',
        tratamientoDP: '',
        puestoDP: '',
        //pagina 02
        calleC: '',
        numeroExteriorC: '',
        numeroInteriorC: '',
        sinNumeroC: '',
        codigoPostalC: '',
        entidadFederativaC: '',
        municipioC: '',
        coloniaC: '',
        correoC: '',
        telefonoC: '',
        //auxiliares
        apiCodigo: false,
        colonias: '',
    }

    /**
    * metodos botones
    */

    changeCargo = e => {
        console.log("cargo ", e)
    }

    /**
     * optencion de dato cuando cambia 
     * @param {*} e 
     */
    changeGenero = e => {
        //console.log(e)
        console.log("genero ", e.target.value)
    }

    /**
     * 
     * @param {*} data 
     * @param {*} dateString texto con el formato 
     */
    changeFechadeNacimiento = (data, dateString) => {
        console.log("fecha Nacimeinto ", dateString)
    }

    changeEntidad = e => {
        console.log("enrtidad ", e)
    }

    changeMunicipio = e => {
        console.log("Municipio ", e)
    }

    changeProfesion = e => {
        console.log("profesion ", e)
    }

    changeTratamiento = e => {
        console.log("tratamiento ", e)
    }

    onChangeCheckBoxNum = e => {
        console.log("checbox ", e)
    }

    onChangeCodigoPostal = e => {
        console.log("codigo postal, ", e)

    }

    /**
     * @param {*} data 
     * @param {*} dateString  el texto con el formato 
     */
    changeFechaIngreso = (data, dateString) => {
        console.log("fehca Ingreso ", dateString)
    }

    /**
     * 
     * @param {*} data 
     * @param {*} dateString el texto con el formato 
     */
    changeFechaNombramiento = (data, dateString) => {
        console.log("fecha Nombramiento ", dateString)
    }

    /**
     * 
     * @param {*} data 
     * @param {*} dateString 
     */
    changeFechaTitulariad = (data, dateString) => {
        console.log("fecha de titulariad ", dateString);
    }

    /**
     * 
     * @param {*} e 
     */
    changeFechaInicio = (data, dateString) => {
        console.log("fecha inicio ", dateString)
    }
    /**
     * 
     * @param {*} e 
     */
    changeFechaFinal = (data, dateString) => {
        console.log("fecha final", dateString)
    }
    /**
     * 
     * @param {*} e 
     */
    changeRama = e => {
        console.log("rama", e)
    }

    changePuestoExperiencia = e => {
        console.log("puesto ", e)
    }
    
    changeInstituto = e => {
        console.log("Instituto", e)
    }

    changeEntidadFederativa = e => {
        console.log("entidad Federativ ", e)
    }
    changeDistrito = e => {
        console.log("distrito ", e)

    }

    next = () => {
        this.setState({ pagina: this.state.pagina + 1 });

    }

    prev = () => {
        this.setState({ pagina: this.state.pagina - 1 });
    }

    enviar = () => {
        let data = {
            /**
             * pagina 01
             */
            "fotografia": "",
            "cargo": "",
            "apellidoPaterno": "",
            "apellidoMaterno": "",
            "nombre": "",
            "claveElector": "",
            "curp": "",
            "genero": "",
            "fechaNacimiento": "",
            "ciudadanoMexicano": "",
            "entidadNacimiento": "",
            "municipioNacimiento": "",
            "profesionPersonales": "",
            "tratamiento": "",
            "Puesto": "",
            /**
             * pagina 02
             */
            "calle": "",
            "mumeroExterior": "",
            "nuemroInterior": "",
            "sinNumero": "",
            "codigoPostal": "",
            "entidadFederativa": "",
            "municipio": "",
            "colonia": "",
            "correo": "",
            "telefono": "",
            /**
             * pagina 03
             */
            "fechaIngreso": "",
            "fechaNombramiento": "",
            "fechaTitularidad": "",
            "nombramiento": "",
            "puesto": "",
            "firma": "",
            /**
             * pagina 04
             */
            "fechaInicio": "",
            "fechaTermino": "",
            "rama": "",
            "puestoExperiencia": "",
            "instituto": "",
            "entidadFederativaExperiencia": "",
            "distritoExperiencia": ""
        }
    }

    render() {
        
        /**
         * valida si lo que ingresa es valido 
         * @param {*} content 
         * @param {*} id 
         * @param {*} name 
         * @param {*} regex 
         * @param {*} warningMessage 
         * @param {*} disabled 
         */
        const handleInput = (content, id, name, regex, warningMessage, disabled) => {

        }

        return (
            <div className="containerLista container" >
                <h1>Nuevo Registro de Vocal</h1>
                <h6></h6>

                <div>
                    <Steps current={this.state.pagina}>

                        <Step title="Datos Personales" />
                        <Step title="Contacto" />
                        <Step title="Estatus" />
                        <Step title="Experiencia" />

                    </Steps>
                    {/*
                        pagina datos Personales
                    */}
                    {this.state.pagina === 0 &&
                        <div className="col-sm-6">
                            <br/>
                            <div className="col-sm-12">
                                *Cargo
                                <br />
                                <Select className="tamaño-select" onChange={this.changeCargo}
                                    placeholder="Selecciona una opción">

                                    <Option value="jack">Jack</Option>
                                    <Option value="lucy">Lucy</Option>
                                    <Option value="tom">Tom</Option>
                                </Select>
                            </div>
                            <br/>
                            <InputINE className='ant-input-sinIconoIzquierdo' onSelectLanguage={handleInput}
                                label='*Apellido paterno' name='calle'
                                placeHolder='Apellido paterno'
                                value={''}
                                type='text'
                                regex={/^([\w. áéíóúÁÉÍÓÚñ.,-;#’/])+$/}
                                // icon={<LockOutlined style={{ color: '#d5007f' }} />}
                                warningMessage='Apellido paterno con caracteres inválidos'
                                tooltipMessage='Apellido paterno'
                                paddingLeft='3px' />
                            <br/>
                            <InputINE className='ant-input-sinIconoIzquierdo' onSelectLanguage={handleInput}
                                label='*Apellido materno' name='apellidoMaterno'
                                placeHolder='Apellido materno'
                                value={''}
                                type='text'
                                regex={/^([\w. áéíóúÁÉÍÓÚñ.,-;#’/])+$/}
                                // icon={<LockOutlined style={{ color: '#d5007f' }} />}
                                warningMessage='Apellido materno con caracteres inválidos'
                                tooltipMessage='Apellido materno'
                                paddingLeft='3px' />
                            <br/>
                            <InputINE className='ant-input-sinIconoIzquierdo' onSelectLanguage={handleInput}
                                label='* Nombre(s)' name='Nombre'
                                placeHolder='Nombre(s)'
                                value={''}
                                type='text'
                                regex={/^([\w. áéíóúÁÉÍÓÚñ.,-;#’/])+$/}
                                // icon={<LockOutlined style={{ color: '#d5007f' }} />}
                                warningMessage=' Nombre(s) con caracteres inválidos'
                                tooltipMessage=' Nombre(s)'
                                paddingLeft='3px' />
                            <br/>
                            <InputINE className='ant-input-sinIconoIzquierdo' onSelectLanguage={handleInput}
                                label='* Clave de elector' name='Clave de elector'
                                placeHolder='AAAAAA12345678A123'
                                value={''}
                                type='text'
                                regex={/^([\w. áéíóúÁÉÍÓÚñ.,-;#’/])+$/}
                                // icon={<LockOutlined style={{ color: '#d5007f' }} />}
                                warningMessage='Clave de elector con caracteres inválidos'
                                tooltipMessage='Clave de elector'
                                paddingLeft='3px' />
                            <br/>
                            <InputINE className='ant-input-sinIconoIzquierdo' onSelectLanguage={handleInput}
                                label='*CURP' name='CURP'
                                placeHolder='AAAA123456AAABBB01'
                                value={''}
                                type='text'
                                regex={/^([\w. áéíóúÁÉÍÓÚñ.,-;#’/])+$/}
                                // icon={<LockOutlined style={{ color: '#d5007f' }} />}
                                warningMessage='CURP con caracteres inválidos'
                                tooltipMessage='CURP'
                                paddingLeft='3px' />
                            <br/>
                            <div>
                                *Genero
                                <br />
                                <Radio.Group onChange={this.changeGenero} >
                                    <Radio value={1}>Femenino</Radio>
                                    <Radio value={2}>Masculino</Radio>

                                </Radio.Group>
                            </div>
                            <br/>
                            <div>
                                *Fecha de nacimiento
                                    <br />
                                <DatePicker placeHolder={"DD/MM/AAAA"} format={'DD/MM/YYYY'} onChange={(evento, fecha) => this.changeFechadeNacimiento(evento, fecha)} />
                            </div>
                            <br/>
                            <div>
                                *Ciudadana/o mexicana/o
                                    <br />
                                <Radio.Group onChange={this.changeGenero} >
                                    <Radio value={1}>Nacida/o en territorio mexicano</Radio>
                                    <Radio value={2}>Nacida/o en el extranjero</Radio>
                                    <Radio value={3}>Nacionalizada/o</Radio>
                                </Radio.Group>
                            </div>
                            <br/>
                            <div className="col-sm-12">
                                *Entidad Federativa de nacimiento
                                    <br />
                                <Select className="tamaño-select" onChange={this.changeEntidad}
                                    placeholder="Selecciona una opción">
                                    <Option value="jack">Jack</Option>
                                    <Option value="lucy">Lucy</Option>
                                    <Option value="tom">Tom</Option>
                                </Select>
                            </div>
                            <br/>
                            <div className="col-sm-12">
                                *Municipio de nacimiento
                                    <br />
                                <Select className="tamaño-select" onChange={this.changeMunicipio}
                                    placeholder="Selecciona una opción">
                                    <Option value="jack">Jack</Option>
                                    <Option value="lucy">Lucy</Option>
                                    <Option value="tom">Tom</Option>
                                </Select>
                            </div>
                            <br/>
                            <div className="col-sm-12">
                                Profesión
                                    <br />
                                <Select className="tamaño-select" onChange={this.changeProfesion}
                                    placeholder="Selecciona una opción">
                                    <Option value="jack">Jack</Option>
                                    <Option value="lucy">Lucy</Option>
                                    <Option value="tom">Tom</Option>
                                </Select>
                            </div>
                            <br/>
                            <div className="col-sm-12">
                                Tratamiento
                                    <br />
                                <Select className="tamaño-select" onChange={this.changeTratamiento}
                                    placeholder="Selecciona una opción">
                                    <Option value="jack">Jack</Option>
                                    <Option value="lucy">Lucy</Option>
                                    <Option value="tom">Tom</Option>
                                </Select>
                            </div>
                            <br/>
                            <InputINE className='ant-input-sinIconoIzquierdo' onSelectLanguage={handleInput}
                                label='Puesto que ejerce' name='Puesto que ejerce'
                                placeHolder='Ingresa información'
                                value={''}
                                type='text'
                                regex={/^([\w. áéíóúÁÉÍÓÚñ.,-;#’/])+$/}
                                // icon={<LockOutlined style={{ color: '#d5007f' }} />}
                                warningMessage='Puesto que ejerce con caracteres inválidos'
                                tooltipMessage='Puesto que ejerce'
                                paddingLeft='3px' />
                        </div>
                    }
                    {/*
                        Contacto 
                    */}
                    {this.state.pagina === 1 &&
                        <div className="steps-content">
                            <h3>Domicilio</h3>
                                <div class="form-row mt-4">
                                    <div class="col-sm-4 pb-3">
                                        <InputINE className='ant-input-sinIconoIzquierdo' onSelectLanguage={handleInput}
                                            label='*Calle' name='calle'
                                            placeHolder='Ingresa calle'
                                            value={''}
                                            type='text'
                                            regex={/^([\w. áéíóúÁÉÍÓÚñ.,-;#’/])+$/}
                                            // icon={<LockOutlined style={{ color: '#d5007f' }} />}
                                            warningMessage='Calle con caracteres inválidos'
                                            tooltipMessage='Calle'
                                            paddingLeft='3px' />
                                    </div>
                                    <div class="col-sm-2 pb-3">
                                        {/* {console.log("quitar ", checkBoxNum)} */}
                                        {this.state.checkBoxNum === true ?
                                            <div>
                                                No. exterior
                                                <p className='h45'>S/N</p>
                                            </div>
                                            :
                                            <InputINE onSelectLanguage={handleInput}
                                                label='*No. exterior' name='numeroExt'
                                                placeHolder='Número ext.'
                                                value={''}
                                                type='text'
                                                regex={/^\d+$/}
                                                // icon={<LockOutlined style={{ color: '#d5007f' }} />}
                                                warningMessage='Número exterior sólo debe contener números'
                                                tooltipMessage='Número exterior'
                                                paddingLeft='3px'

                                            />
                                        }
                                    </div>
                                    <div class="col-sm-2 pb-3">
                                        {this.state.checkBoxNum === true ?
                                            <div>
                                                No. interior
                                                <p className='h45'>S/N</p>
                                            </div>
                                            :
                                            <InputINE onSelectLanguage={handleInput}
                                                label='*No. interior' name='numeroInt'
                                                placeHolder='Número int.'
                                                value={''}
                                                type='text'
                                                regex={/^\d+$/}
                                                // icon={<LockOutlined style={{ color: '#d5007f' }} />}
                                                warningMessage='Número interior sólo debe contener números'
                                                tooltipMessage='Número interior'
                                                paddingLeft='3px'
                                            />
                                        }
                                    </div>
                                    {/* checked={numeroExterior==-1&&numeroInterior==-1?true:false} */}
                                    <div class="col-sm-2 pb-3">
                                        <Checkbox style={{ paddingTop: '36px' }} onChange={this.onChangeCheckBoxNum}>Sin número</Checkbox>

                                    </div>
                                    <div class="col-sm-2 pb-3">
                                        <InputINE onSelectLanguage={handleInput}
                                            onChange={this.onChangeCodigoPostal}

                                            label='*C.P.' name='codigoPostal'
                                            placeHolder='12345'
                                            value={''}
                                            type='text'
                                            regex={/^\d{5}$/}
                                            // icon={<LockOutlined style={{ color: '#d5007f' }} />}
                                            warningMessage='Código Postal sólo debe contener números'
                                            tooltipMessage='Código Postal'
                                            paddingLeft='3px'
                                            style={{ disabled: true }}
                                        />
                                    </div>
                                </div>
                                
                                <div class="form-row mt-4">
                                    <div class="col-sm-4 pb-3">
                                        {this.state.apiCodigo ?
                                            <div>
                                                *Entidad
                                                <p className='h45'>{null}</p>
                                            </div>
                                            :
                                            <InputINE onSelectLanguage={handleInput}
                                                label='*Entidad' name='entidad'
                                                placeHolder='Ingresa Entidad'
                                                type='text'
                                                regex={/^([\w. áéíóúÁÉÍÓÚñ.,-;#’/])+$/}
                                                value={''}
                                                // icon={<LockOutlined style={{ color: '#d5007f' }} />}
                                                warningMessage='Entidad tiene caracteres inválidos'
                                                tooltipMessage='Entidad '
                                                paddingLeft='3px' />
                                        }
                                    </div>

                                    <div class="col-sm-4 pb-2">
                                        {this.state.apiCodigo ?
                                            <div>
                                                *Municipio
                                                <p className='h45'>{null}</p>
                                            </div>
                                            :
                                            <InputINE onSelectLanguage={handleInput}
                                                label='*Municipio' name='municipio'
                                                placeHolder='Ingresa Municipio'
                                                type='text'
                                                regex={/^([\w. áéíóúÁÉÍÓÚñ.,-;#’/])+$/}
                                                // icon={<LockOutlined style={{ color: '#d5007f' }} />}
                                                value={''}
                                                warningMessage='Municipio tiene caracteres inválidos'
                                                tooltipMessage='Municipio'
                                                paddingLeft='3px' >
                                            </InputINE>
                                        }
                                    </div>
                                    <div class="col-sm-4 pb-2">
                                        *Colonia
                                    {
                                        this.state.apiCodigo ?

                                        // <Select style={{ width: '100%' }} onChange={selectColonia} >
                                        //     {colonias.map(tem =>

                                        //         <Select.Option value={tem}>{tem}</Select.Option>

                                        //     )}
                                        // </Select>
                                        <h1>hola</h1>
                                        :
                                        <InputINE onSelectLanguage={handleInput}
                                            label='' name='colonia'
                                            placeHolder='Ingresa tu colonia'
                                            type='text'
                                            value={''}
                                            regex={/^([\w. áéíóúÁÉÍÓÚñ.,-;#’/])+$/}
                                            warningMessage='Colonia tiene caracteres inválidos'
                                            tooltipMessage='Colonia'
                                            paddingLeft='3px' />
                                    }

                                    </div>
                                </div>

                                <h3>Contacto</h3>
                                <div class="form-row mt-4">
                                    <div class="col-sm-4 pb-3">
                                        <InputINE onSelectLanguage={handleInput}
                                            label='*Correo' name='correo'
                                            placeHolder='nombre@dominio.com'
                                            value={''}
                                            type='text'
                                            regex={/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/}
                                            // icon={<LockOutlined style={{ color: '#d5007f' }} />}
                                            warningMessage='Correo tiene caracteres inválidos'
                                            tooltipMessage='Correo'
                                            paddingLeft='3px' />
                                    </div>
                                    <div class="col-sm-1 pb-2">
                                        <InputINE onSelectLanguage={handleInput}
                                            label='Lada' name='lada'
                                            placeHolder='55'
                                            value={''}
                                            type='text'
                                            regex={/^\d{1,3}$/}
                                            // icon={<LockOutlined style={{ color: '#d5007f' }} />}
                                            warningMessage='Lada inválida'
                                            tooltipMessage='Lada'
                                            paddingLeft='3px' />
                                    </div>
                                    <div class="col-sm-2 pb-2">
                                        <InputINE onSelectLanguage={handleInput}
                                            label='*Teléfono' name='telefono'
                                            placeHolder='0012345678'
                                            value={''}
                                            type='text'
                                            regex={/^\d{7,8}$/}
                                            // icon={<LockOutlined style={{ color: '#d5007f' }} />}
                                            warningMessage='Teléfono tiene caracteres inválidos'
                                            tooltipMessage='Teléfono'
                                            paddingLeft='3px' />
                                    </div>
                                </div>
                                <div><p /><p /><p /></div>
                                
                        </div>
                    }
                    {/*
                        Estatus
                    */}
                    {this.state.pagina === 2 &&
                        <div className="steps-content">
                            <br/>
                            <div>Estatus </div>
                            <div className="col-sm-6">
                                <br/>
                                <div>
                                    *Fecha de nacimiento
                                    <br />
                                    <DatePicker placeHolder={"DD/MM/AAAA"} format={'DD/MM/YYYY'} onChange={(evento, fecha) => this.changeFechaIngreso(evento, fecha)} />
                                </div>
                                <br/>
                                <div>
                                    *Fecha de ingreso
                                    <br />
                                    <DatePicker placeHolder={"DD/MM/AAAA"} format={'DD/MM/YYYY'} onChange={(evento, fecha) => this.changeFechaIngreso(evento, fecha)} />
                                </div>
                                <br/>
                                <div>
                                    Fecha de nombramiento
                                    <br />
                                    <DatePicker placeHolder={"DD/MM/AAAA"} format={'DD/MM/YYYY'} onChange={(evento, fecha) => this.changeFechaNombramiento(evento, fecha)} />
                                </div>
                                <br/>
                                <div>
                                    Fecha de titularidad
                                    <br />
                                    <DatePicker placeHolder={"DD/MM/AAAA"} format={'DD/MM/YYYY'} onChange={(evento, fecha) => this.changeFechaTitulariad(evento, fecha)} />
                                </div>
                                <br/>
                                <div>
                                    *Genero
                                    <br />
                                    <Radio.Group onChange={this.changeGenero} >
                                        <Radio value={1}>Propietario</Radio>
                                        <Radio value={2}>Encargado del Despacho</Radio>

                                    </Radio.Group>
                                </div>
                                <br/>
                            </div>
                        </div>
                    }
                    {/*
                        experiencia 
                    */}
                    {this.state.pagina === 3 &&
                        <div className="steps-content">
                            <br/>
                            <div>
                                *Fecha de inicio
                                <br />
                                <DatePicker placeHolder={"DD/MM/AAAA"} format={'DD/MM/YYYY'} onChange={(evento, fecha) => this.changeFechaInicio(evento, fecha)} />
                            </div>
                            <br/>
                            <div>
                                *Fecha de término
                                <br />
                                <DatePicker placeHolder={"DD/MM/AAAA"} format={'DD/MM/YYYY'} onChange={(evento, fecha) => this.changeFechaFinal(evento, fecha)} />
                            </div>
                            <br/>
                            <div>
                                Rama
                                    <br />
                                <Radio.Group onChange={this.changeRama} >
                                    <Radio value={1}>Administrativo</Radio>
                                    <Radio value={2}>Servicio Profesional</Radio>
                                </Radio.Group>
                            </div>
                            <br/>
                            <div className="col-sm-12">
                                *Puesto
                                    <br />
                                <Select onChange={this.changePuestoExperiencia}
                                    placeholder="Selecciona una opción">
                                    <Option value="jack">Jack</Option>
                                    <Option value="lucy">Lucy</Option>
                                    <Option value="tom">Tom</Option>
                                </Select>
                            </div>
                            <br/>
                            <div>
                                Instituto
                                    <br />
                                <Radio.Group onChange={this.changeRama} >
                                    <Radio value={1}>INE</Radio>
                                    <Radio value={2}>OPL</Radio>
                                </Radio.Group>
                            </div>
                            <br/>
                            <div className="col-sm-6">
                                Entidad Federativa
                                    <br />
                                <Select className="tamaño-select" onChange={this.changeEntidadFederativa}
                                    placeholder="Selecciona una opción">
                                    <Option value="jack">Jack</Option>
                                    <Option value="lucy">Lucy</Option>
                                    <Option value="tom">Tom</Option>
                                </Select>
                            </div>
                            <br/>
                            <div className="col-sm-6">
                                Distrito
                                    <br />
                                <Select className="tamaño-select" onChange={this.changeDistrito}
                                    placeholder="Selecciona una opción">
                                    <Option value="jack">Jack</Option>
                                    <Option value="lucy">Lucy</Option>
                                    <Option value="tom">Tom</Option>
                                </Select>
                            </div>
                            <br/>
                        </div>
                    }

                    <div className="steps-action">
                        {this.state.pagina >= 1 &&
                            <Button type="primary" onClick={this.prev}>
                                anterior
                            </Button>
                        }
                        {this.state.pagina < 3 && this.state.pagina >= 0 &&
                            <Button type="primary" onClick={this.next}>
                                Siguiente
                            </Button>
                        }

                        {
                            this.state.pagina === 3 &&
                            <Button type="primary" onClick={this.enviar}>
                                Enviar
                            </Button>
                        }
                    </div>
                    <br/>


                </div>

            </div>
          );
    }

}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(VocalesFormulario))