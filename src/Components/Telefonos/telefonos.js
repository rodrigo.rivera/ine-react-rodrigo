import React, { useState, useEffect } from "react";
import "antd/dist/antd.css";
import { List, Select, Input, Button } from "antd";
const { Option } = Select;

export default function Telefonos(props) {
  const { getData, lista } = props;

  const [listaNumero, setLista] = useState(lista != null ? lista : []);

  const [tipo, setTipo] = useState(1);
  //const [cont] = useState(lista!= null ?listaNumero.length+1:0);
  const [telefonoValido, setTelefono] = useState({
    status: false,
    exito: false,
    telefono: "",    
  });

  useEffect(()=>{
    getData(listaNumero);
  },[listaNumero])
  const addTelefono = e => {
    console.log("add", telefonoValido);
    let buscar = listaNumero.find(
      element => element.telefono == telefonoValido.telefono
    );
    console.log("busca ", buscar)
    if (buscar == undefined) {
      setLista(
        listaNumero.concat({ telefono: telefonoValido.telefono,tipo: tipo })
      );
      setTelefono({ status: true, exito: false, telefono: "" });
    } else {
      alert("el numero ya esta duplicado")
      setTelefono({ status: false, exito: false, telefono: "" });
    }
  };
  const agregarNumero = e => {
    let numeroT = e.target.value;
    //console.log("entro", e.target.value);
    setTelefono({ status: true, telefono: numeroT });
    if ( numeroT.length == 10) {
      setTelefono({ status: true, exito: true, telefono: parseInt(numeroT) });
    }
  };
  const cahngeTipo = e => {
    console.log(e);
    setTipo(e);
  };
  const eliminar = (e, id) => {
    //console.log("1se va ", Object.keys(e.target));    
    setLista(listaNumero.filter(item => item.telefono != id.telefono));
    //setLista(listaNumero.filter(item => item.key !== key));
  };
  return (
    <div>
      <Input.Group compact>
        <Select defaultValue="Móvil" onChange={cahngeTipo}>
          <Option value={1}>Móvil</Option>
          <Option value={2}>Casa</Option>
        </Select>
        <Input
          style={{ width: "50%" }}
          id="icon_prefix"
          type="number"
          value={telefonoValido.status ? telefonoValido.telefono : null}
          onChange={agregarNumero}
          class="validate"
        />
        <p onClick={addTelefono} hidden={!telefonoValido.exito}>
          <i className="iconAdd" />
        </p>
      </Input.Group>
      <br />
      <br />

      <List
        className="demo-loadmore-list"
        itemLayout="horizontal"
        split={false}
        dataSource={listaNumero}
        renderItem={item => (
          <List.Item
            actions={[
              <p
                onClick={it => eliminar(it, item)}
                id={item.telefono}
                key="list-loadmore-edit"
              >
                <i className="iconDelete" />
              </p>
            ]}
          >
            {item.tipo==1?"Móvil":"Casa"} {item.telefono}
          </List.Item>
        )}
      />
    </div>
  );
}