import React, { useState, useEffect } from 'react';

import 'antd/dist/antd.css';
//import './index.css';
import { Upload, message, Button, Tooltip, Spin, Icon } from 'antd';
import { UploadOutlined } from '@ant-design/icons';
import alertINE from "../../Components/Alert/AlertsINE";
const antIcon = <Icon type="loading" style={{ fontSize: 30 }} spin />;

function getBase64(img, callback) {
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result));
    reader.readAsDataURL(img);
}

function beforeUpload(file) {
    // console.log("archvo ", file)
    // console.log(" typo de ", file.type)    
    const isJpgOrPng = file.type == 'application/pdf';
    if (!isJpgOrPng) {
        alertINE("error", "Solo soporta Archvios  PDF!");
    }
    const isLt2M = file.size / 1024 / 1024 < 2;
    if (!isLt2M) {
        alertINE("error", "El pdf es muy grnade solo 2MB");
    }
    return isJpgOrPng && isLt2M;
}

export default function CargarArchivos(props) {

    const { getData, ponerArchivo, editar } = props
    //console.log("los props omgen  ", props)
    const [loading, setLoading] = useState(false);

    const [imageUrl, setImageUrl] = useState(null);
    const [nombre, setNombre] = useState(props.ponerArchivo ? props.ponerArchivo.name : null);


    const handleChange = info => {
        if (info.file.status === 'uploading') {
            //this.setState({ loading: true });

            setLoading(true)
            setNombre("")

            return;
        }
        if (info.file.status === 'done') {


            //console.log("valoe", info.file)
            getData(info.file);
            setNombre(info.file.name)
            setLoading(false)

            // getBase64(info.file.originFileObj, imageUrl => {
            //     setImageUrl(imageUrl)
            //     setLoading(false)
            // }
            //);
        }
    };


    return (
        <div>
            <div hidden={loading}>
                <Upload
                    // name="avatar"
                    // listType="picture-card"
                    // className="avatar-uploader"
                    showUploadList={false}
                    action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
                    beforeUpload={beforeUpload}
                    onChange={handleChange}
                    hidden={loading}
                >

                    <Button >
                        <UploadOutlined /> Subir

                        </Button>
                    <Tooltip placement="right" title="Solo se aceptan PDF de tamaño menores a 2MB">
                        <i className="tooltip-apellido" />
                    </Tooltip>
                </Upload>
            </div>

            <br />
            {nombre ? <><i className="iconClip" />{nombre}</> :
                editar != undefined ?
                    <a href={editar} download  >   <i className="iconClip" /><a className="link">Nombramiento.pdf</a> </a> :
                     null}
            <br />
            {loading ?
                <div className="loading-style">
                    <Spin indicator={antIcon} tip="Cargando Archvio"></Spin>
                </div>
                : null
            }
        </div>
    );

}

