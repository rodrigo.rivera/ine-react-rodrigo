import React, { useState } from "react";
import { Table, Avatar, Steps, Button, Select, Radio, DatePicker, Checkbox } from 'antd';
const { Column } = Table;


export function TablaRegistro(props) {

    const { data } = props
    console.log("props TablaRegistro")

    return (
        <div>
            {console.log("las llaves ", Object.keys(data[0]))}
            <Table dataSource={data} theme="dark" pagination={false}>
                <Column title="Fecha inicio" dataIndex="fechaIncio" key="fechaIncio" />
                <Column title="Fecha término" dataIndex="fechaTermino" key="fechaTermino" />
                <Column title="Rama" dataIndex="rama" key="rama" />
                <Column title="Puesto" dataIndex="puesto" key="puesto" />
                <Column title="Instituto" dataIndex="instituto" key="instituto" />
                <Column title="Entidad" dataIndex="entidad" key="entidad" />
                <Column title="Distrito" dataIndex="distrito" key="distrito" />
                {data.acciones == undefined ? <Column title="Acciones" dataIndex="acciones" key="acciones" /> : null}

            </Table>
        </div>
    );

}

export function TablaMostrar(props) {

}