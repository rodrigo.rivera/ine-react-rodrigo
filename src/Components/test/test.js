import React, { useState, useEffect } from 'react';
import axios from "axios";
import Home from "../../PaginasContenedores/Vocales/VocalesHome";
import { BrowserRouter as Router, Route } from "react-router-dom";

export default function Test({props}) {
    const [birtacora, setBitacora] = useState(null);
    const [dataMostrar, setMostrar] = useState("rodriogo");
    const [profesiones, setProfesiones] = useState(null);
    const [tratamiento, setTratamiento] = useState(null);
    const [motivosBaja, setMotivosBaja] = useState(null);
    const [error, setError] = useState(false);
    console.log("test", props);

    useEffect(() => {
        console.log("entada");
        const datFetch = async () => {
            try {
                const repuesta = await axios.post("https://localhost:8443/centralSesiones/vocales/consulta", {
                    "idDetalleProceso": 106,
                    "idDistrito": 0,
                    "idEstado": 5,
                });
                console.log("vocales ", repuesta);
                setBitacora(repuesta.birtacora != null ? repuesta.birtacora : null);
                setMotivosBaja(repuesta.motivosBaja != null ? repuesta.motivosBaja : null);
                setProfesiones(repuesta.profesiones != null ? repuesta.profesiones[0] : null);
                setTratamiento(repuesta.tratamiento != null ? repuesta.tratamiento : null);
                setMostrar(repuesta.vocalesHome != null ? repuesta.vocalesHome : null);
                setError(false);

            } catch (error) {

                console.log("error ", error)
            }
        }
        datFetch();

        console.log("salida");

    }, []);

    return (

        <>
            {JSON.stringify(props)}
            <Route path="/vocales/home" >
                <Home  />
            </Route>
        </>
    )
}