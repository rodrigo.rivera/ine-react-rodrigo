/**
 * Propósito: Codificar en base 64 un cadena.
 * @author karen.vazquez, INE
 * @param {*} texto  Texto a codificar.
 */
export const codificarBase64 = (texto) =>{

    return btoa(texto);

}

/**
 * Propósito: Decodificar en base 64 una cadena de texto
 * @author karen.vazquez, INE
 * @param {*} texto  Texto a decodificar
 */
export const decodificarBase64 = (texto) => {

    return atob(texto);

}