import React, { Component } from "react";
import "antd/dist/antd.css";
import "bootstrap/dist/css/bootstrap.min.css";
import "../src/Assets/css/index.css";
import Main from "./Components/MainComponent";

import { ConfigureStore } from "./Configurations/ConfigureStore";
// import {
//   BrowserRouter as Router,
//   Switch,
//   Route,

// } from "react-router-dom";
// import PaginaError from "./PaginasContenedores/Pagina400";

const store = ConfigureStore();

class App extends Component {
  render() {
    return <Main></Main>;
  }
}

export default App;
