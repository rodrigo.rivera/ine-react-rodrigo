import { createStore, applyMiddleware ,compose} from 'redux';
import thunk from 'redux-thunk';
import {cargarEstado, guardarEstado} from './SessionStorage'
import rootReducer from '../Reducers/CombinedReducers'

const estado = cargarEstado();
export const ConfigureStore = () => {

    const store = createStore(
        rootReducer,
        estado,
        compose(
          applyMiddleware(thunk)
        )
      )

      store.subscribe( function () {

        console.log(store.getState());
        guardarEstado(store.getState())
      })

    return store;
};